<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productos', function (Blueprint $table) {
            // $table->id();
            $table->increments('id');
            $table->string('Nombre',45);
            $table->Integer('Cantidad');
            $table->double('Precio');;
            $table->integer('Imagenes')->nullable();
            $table->mediumText('Descripcion');
            $table->enum('Estado', ['Habilitado', 'Deshabilitado'])->default('Habilitado');
            $table->Integer('TotalVentas')->nullable();
            $table->integer('popularity')->default(1);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productos');
    }
}
