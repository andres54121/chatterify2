<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carts', function (Blueprint $table) {
            $table->increments('id');

            $table->string('code')->nullable();
            $table->string('order_date')->nullable();          # Fecha de cuando se desea recibir el pedido
            $table->string('arrived_date')->nullable();        # Fecha de llegada del pedido
            $table->string('status');

            $table->double('total', 12, 2)->nullable();
            $table->integer('address_id')->nullable()->references('id')->on('addresses')->onDelete('cascade');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carts');
    }
}
