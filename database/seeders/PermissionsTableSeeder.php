<?php

namespace Database\Seeders;

use App\Http\Controllers\admin\UserController;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Usuarios
        // Permission::updateOrCreate(
        //     [ 'name' => UserController::PERMISSIONS['create'] ],
        //     [ 'description' => 'Creacion de usuarios' ],
        //     // [ 'guard_name' => '' ],
        //     // [ '' => '' ],
        // );
        // Permission::updateOrCreate(
        //     [ 'name' => UserController::PERMISSIONS['edit'] ],
        //     [ 'description' => 'Editar usuarios' ],
        // );
        // Permission::updateOrCreate(
        //     [ 'name' => UserController::PERMISSIONS['destroy'] ],
        //     [ 'description' => 'Eliminar usuarios' ],
        // );

        // Plantas
        Permission::updateOrCreate(
            [ 'name' => 'planta.create' ],
            [ 'description' => 'Agregar una planta al catalogo.' ],
        );
        Permission::updateOrCreate(
            [ 'name' => 'planta.edit' ],
            [ 'description' => 'Editar una planta del catalogo.' ],
        );
        Permission::updateOrCreate(
            [ 'name' => 'planta.destroy' ],
            [ 'description' => 'Eliminar una planta del catalogo.' ],
        );

        // Productos
        Permission::updateOrCreate(
            [ 'name' => 'producto.create' ],
            [ 'description' => 'Agregar un producto al catalogo.' ],
        );
        Permission::updateOrCreate(
            [ 'name' => 'producto.edit' ],
            [ 'description' => 'Editar un producto del catalogo.' ],
        );
        Permission::updateOrCreate(
            [ 'name' => 'producto.destroy' ],
            [ 'description' => 'Eliminar un producto del catalogo.' ],
        );

        // Pedidos
        Permission::updateOrCreate(
            [ 'name' => 'pedido.status' ],
            [ 'description' => 'Permite cambiar el status de un pedido.' ],
        );
        // Permission::updateOrCreate(
        //     [ 'name' => '' ],
        //     [ 'description' => '' ],
        // );
    }
}
