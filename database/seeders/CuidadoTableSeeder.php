<?php

namespace Database\Seeders;

use App\Models\Cuidado;
use Illuminate\Database\Seeder;

class CuidadoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Cuidado::Create(
            [
                'Tipo' => 'Arbol',
                'Descripcion' => "Fertilización: tanto en el momento de la plantación como después durante el mantenimiento, el árbol frutal necesita abono con altos nutrientes y con la tierra normal del campo no es necesario.
            Poda:  Es absolutamente necesario conseguir una estructura abierta del ramaje del árbol frutal para conseguir recolectar el fruto 100% ideal.
            Reproducción: cuando un árbol frutal  o comun sale rentable y de buena calidad suele multiplicarse por alguna técnica de reproducción como puede ser por semillas o esquejes, incluso por injerto.
            Protección: deben eliminarse todos los microorganismos que puedan afectar a nuestro árbol  y prevenir su aparición con diferentes productos que pueden encontrarse en cualquier tienda especializada."
             ],
        );
        Cuidado::Create(
            [
                'Tipo' => 'Arbustos',
                'Descripcion' => "Regar copiosamente una vez por semana o cada quince días ( de acuerdo a la estación del año y el tipo de suelo deonde han sido plantados) durante los dos primeros años de vida.
            Apisonar la tierra o verificar que esté firme después de alguna tormenta.
            Controlar los hormigueros.
            Mantener las hoyas o palanganas limpias para evitar lastimaduras cpn las bordeadoras, además de mejorar la absorción de agua y nutrientes."
             ],
        );
        Cuidado::Create(
            [
                'Tipo' => 'Trepadoras',
                'Descripcion' => "Riego: Una de las principales causas de muerte de este tipo de plantas es el exceso de humedad, ya que es muy fácil que se pudran sus raíces. Además el exceso de agua, hace que no enraíce bien, ya que no necesita unas raíces fuertes y profundas para buscar el agua.
            Poda: Como en toda planta, es necesario podarlo periódicamente, pero en el caso de las trepadoras se distinguen dos tipos de poda:
            Poda de limpieza: Cuando la planta ya es adulta, hay que realizar una poda anual de limpieza o mantenimiento para eliminar zonas muertas, enfermas, ramas cruzadas o mal orientadas. La mejor época es el invierno. El tener una enredadera saneada y mantenida permite que penetre mejor la luz y no envejezca prematuramente.
            Poda de floración: En las especies con flor se realiza una poda adicional. Se realiza una poda después de acabar la floración, para que nazcan los brotes que darán las flores del año próximo. Además, no coincide con la poda de invierno."
             ],
        );
        Cuidado::Create(
            [
                'Tipo' => 'Cactus',
                'Descripcion' => "Riego:  Este se debe de adaptar de acuerdo a la estación del año en que te encuentres,  generoso en tiempos de calor y  lo necesario en época de invierno ya que varios de ellos hibernan en esta época, por lo que te recomendamos que el riego sea cada 30 días en invierno y cada semana en verano.  Es importante evitar que a la hora de regarlos el agua toque el cuerpo de la planta, así que se regará solo alrededor procurando que el agua toque el sitio de las raíces, así mismo, no se debe de inundar la maceta, solo humedecer el sustrato. Ellos son capaces de vivir tiempos prologandos sin agua, pues almacenan en sus raíces y cuerpos el agua para las futuras sequías.
            Luz:  Es importante en cuanto a su crecimiento se refiere, son amantes de la luz y el calor, sin embargo es recomendable que solo tomen sus baños de sol directo de la mañana al medio día, la luz les ayuda a tomar color y a fortalecer sus cuerpos, por lo que es indispensable.
            Sustrato:  Se recomienda que sea permeable y que se encuentre bien drenado para no acumular humedad en exceso, la variedad de la tierra y sustrato dependerá de la especie que hayas elegido, así como de las condiciones climáticas. Cada coleccionista, productor o viverista tiene su fórmula especial de sustrato. La fórmula la eligirás tú según vayas adquiriendo experiencia y conocimiento sobre la especie de cactus que tienes en casa. Podrás ir sustituyendo o modificando los porcentajes.
            Abono:  Se requiere que para ayudarlos a crecer fuertes y sanos :D, se les aplique fertilizante especial para cactus,  puede ser liquido o bien usar el humus de lombriz que les da los nutrientes necesarios, pero esto también variará de acuerdo a la especie.
            Fumigación:  Para evitar enfermedades y plagas es necesario que se realice por lo menos dos veces al año para evitar la proliferación de plagas que puedan dañarlos. De acuerdo a la plaga que presenten será el tipo de fumigación y método que se realice"
             ],
        );
        Cuidado::Create(
            [
                'Tipo' => 'Crasas',
                'Descripcion' => "Riego:  Este se debe de adaptar de acuerdo a la estación del año en que te encuentres,  generoso en tiempos de calor y  lo necesario en época de invierno ya que varios de ellos hibernan en esta época, por lo que te recomendamos que el riego sea cada 30 días en invierno y cada semana en verano.  Es importante evitar que a la hora de regarlos el agua toque el cuerpo de la planta, así que se regará solo alrededor procurando que el agua toque el sitio de las raíces, así mismo, no se debe de inundar la maceta, solo humedecer el sustrato. Ellos son capaces de vivir tiempos prologandos sin agua, pues almacenan en sus raíces y cuerpos el agua para las futuras sequías.
            Luz:  Es importante en cuanto a su crecimiento se refiere, son amantes de la luz y el calor, sin embargo es recomendable que solo tomen sus baños de sol directo de la mañana al medio día, la luz les ayuda a tomar color y a fortalecer sus cuerpos, por lo que es indispensable.
            Sustrato:  Se recomienda que sea permeable y que se encuentre bien drenado para no acumular humedad en exceso, la variedad de la tierra y sustrato dependerá de la especie que hayas elegido, así como de las condiciones climáticas. Cada coleccionista, productor o viverista tiene su fórmula especial de sustrato. La fórmula la eligirás tú según vayas adquiriendo experiencia y conocimiento sobre la especie de cactus que tienes en casa. Podrás ir sustituyendo o modificando los porcentajes.
            Abono:  Se requiere que para ayudarlos a crecer fuertes y sanos :D, se les aplique fertilizante especial para cactus,  puede ser liquido o bien usar el humus de lombriz que les da los nutrientes necesarios, pero esto también variará de acuerdo a la especie.
            Fumigación:  Para evitar enfermedades y plagas es necesario que se realice por lo menos dos veces al año para evitar la proliferación de plagas que puedan dañarlos. De acuerdo a la plaga que presenten será el tipo de fumigación y método que se realice"
             ],
        );
        Cuidado::Create(
            [
                'Tipo' => 'Herbaceas',
                'Descripcion' => "las plantas herbáceas precisan de un suelo bien drenado, por lo que es necesario el riego cada 7 a 10 días. Además, necesita recibir bastante luz solar, pero no de forma directa, ya que el sol podría quemarlas severamente. No toleran temperaturas extremas.

            Por lo regular, son especies resistentes a plagas, por lo que no requieren cuidados extremos. Las herbáceas crecen rápidamente, produciendo semillas en un lapso muy corto. Se adaptan bien a la poda y pueden trasplantarse cada dos años."
             ],
        );
        Cuidado::Create(
            [
                'Tipo' => 'Hortícolas',
                'Descripcion' => "Riegue con regularidad, sobretodo en verano que se necesitará riego todos los días.
            Arranque las malezas, evitando así que se confundan con las nuevas plantas de hortalizas o hierbas.
            Coseche con frecuencia,  pode frutos y flores oxidadas, para así evitar un desgaste de energía a la planta.
            Fertilice, o abone la tierra donde se transplantarán las hortalizas. Evite utilizar abonos frescos como: guanos y compost inmaduros, ya que ésto atrae hongos y al momento de la descomposición absorve el nitrogeno que está disponible para las nuevas plantas .
            Utilice protectores para extender la estación.
            Controle los insectos, con insecticidas orgánicos, para asi evitar esperar consumir éstas hierbas y hortalizas.
            Utilice Tutores, en algunas especies como Habas y tomates, es necesario guiarlos."
             ],
        );
        Cuidado::Create(
            [
                'Tipo' => 'Palmeras',
                'Descripcion' => "Riegos: La mayoría de las personas piensan que porque se trata de palmeras estas no necesitan riegos. Pero la verdad es que las palmeras también necesitan agua, sobre todo al principio de su desarrollo. Una vez establecidas en el terreno se pueden regar solo una vez por semana y hasta mucho menos.  También es bien importante que el terreno tenga buen drenaje. Igual que los excesos en riegos, los encharcamientos de agua son fatales para estas plantas.

            Terreno: El terreno ideal para las palmeras debe mantener buen drenaje. Hay palmeras como las que producen cocos, que pueden vivir en terrenos salinos y bastante húmedos. Pero aún así el terreno debe mantener buen drenaje en las capas superiores, que es donde estas mantienen sus raíces. Los mejores terrenos para la mayoría de las palmeras suelen ser arenosos, sueltos y bien porosos. Pero igual pueden variar de acuerdo al tipo de palmera.

             Mantenimiento general: Las palmeras están entre las plantas más fáciles de mantener dentro o fuera de la casa. Ahora, eso no quiere decir que las podemos dejar en el abandono. Si vives en una zona tropical, posiblemente con mirarla sea suficiente para mantenerla en forma. Pero si vives en una zona templada, necesitarás darle un poco más de cariño.

            Como regla general, asegúrate de plantar tu palmera en un lugar que se mantenga bastante soleado y cálido todo el año. También asegúrate de escoger una palmera que se adapte al clima donde vives. No podes tus palmeras, deja que las hojas se sequen en la misma planta hasta que salgan de un simple tirón. "
             ],
        );
    }
}
