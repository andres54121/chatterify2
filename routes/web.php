<?php

use Illuminate\Support\Facades\Route;

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/dashboard', [App\Http\Controllers\DashboardController::class, 'index'])->name('dashboard');

Route::get('/info', function () {
    return view('info');
});


// Route::get('/users/create', [App\Http\Controllers\UserController::class, 'create']);

Route::middleware(['auth'])->prefix('administracion')->group( function(){
    Route::resource("usuario", "admin\UserController");
    Route::resource("permisos", "admin\PermissionController")->only('index');
    Route::resource("roles", "admin\RoleController");

    Route::resource("plantas", "articulos\PlantaController");
    Route::get('plantas/eliminarimagen/{id}', 'articulos\PlantaController@eliminarimagen')->name('plantas/eliminarimagen');
    Route::post('plantas/changeStatus/{id}', 'articulos\PlantaController@changeStatus')->name('plantas/changeStatus');

    Route::resource("productos", "articulos\ProductoController");
    Route::get('productos/eliminarimagen/{id}', 'articulos\ProductoController@eliminarimagen')->name('productos/eliminarimagen');
    Route::post('productos/changeStatus/{id}', 'articulos\ProductoController@changeStatus')->name('productos/changeStatus');

    Route::get('pedidos', [App\Http\Controllers\Shoppingcart\CartController::class, 'indexUser'])->name('pedidos');
});

#Ruta para agregar un elemento al carrito
Route::post('/cart','Shoppingcart\CartDetailController@store');
#Ruta para eliminar un elemento al carrito
Route::delete('/cart','Shoppingcart\CartDetailController@destroy');
# Ruta para convertir el carrito en un pedido
Route::post('/order','Shoppingcart\CartController@update');
#Ruta para eliminar un pedido
Route::delete('/order','Shoppingcart\CartController@destroy');
#Ruta para cambiar el status de un pedido
Route::post('/order/status/{id})','Shoppingcart\CartController@updateStatus')->name('order/status');
#Ruta para enviar un mensage de consulta al admin
Route::post('/message/new','Shoppingcart\MessageController@store');

// Catalogos
Route::prefix('catalogo')->group( function(){
    Route::get('/plantas','CatalogoController@indexPlantas')->name('plantas');
    Route::post('/plantas','CatalogoController@filtroPlantas')->name('plantas');
    Route::get('/plantas/{id}','CatalogoController@showPlanta');

    Route::get('/herramientas','CatalogoController@indexProductos')->name('herramientas');
    Route::post('/herramientas','CatalogoController@filtroProductos')->name('herramientas');
    Route::get('/herramientas/{id}','CatalogoController@showProducto');
});

// Perfil de usuario
Route::middleware(['auth'])->get('/profile', function () {
    return view('users.profile');
});

Route::middleware(['auth'])->get('/editProfile', function () {
    return view('users.editProfile');
})->name('editProfile');

Route::middleware(['auth'])->get('/changePassword', function () {
    return view('users.changePassword');
})->name('changePassword');
Route::middleware(['auth'])->post('/changePassword','ProfileController@changePassword')->name('changePassword');

Route::middleware(['auth'])->get('/profile','ProfileController@index')->name('profile');
Route::middleware(['auth'])->post('/profile','ProfileController@update')->name('profile');

// Carrito de compras
Route::middleware(['auth'])->get('/Shoppingcart', function () {
    return view('Shoppingcart');
});

// PayPal
Route::post('/paypal/pay', 'PayPal\PaymentController@payWithPayPal')->name('payment');
Route::get('/paypal/status', 'PayPal\PaymentController@payPalStatus');
