<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $fillable = [
        'id',
        'CalleP',
        'entreCalles',
        'Colonia',
        'SMza',
        'Mza',
        'Lt',
        'Descripcion',
        'CP',
        'created_at'
    ];

    public function user(){
        return $this->belongsTo('App\Models\User');
    }

    public function cart(){
        return $this->belongsTo('App\Models\Shoppingcart\Cart');
    }
}
