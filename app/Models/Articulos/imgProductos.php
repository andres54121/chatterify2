<?php

namespace App\Models\Articulos;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class imgProductos extends Model
{
    use HasFactory;
    protected $table = 'img_productos';

    protected $fillable = [
        'nombre',
        'formato',
        'producto_id',
    ];

    public function producto()
    {
    	return $this->belongsTo('App\Models\Articulos\Producto');
    }
}
