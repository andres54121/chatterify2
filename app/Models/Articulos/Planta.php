<?php

namespace App\Models\Articulos;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Planta extends Model
{
    use HasFactory;
    protected $table = 'plantas';

    protected $fillable  = [
        "Id",
        "Nombre",
        "Tipo",
        "Cantidad",
        "Precio",
        "Imagenes",
        "Descripcion",
        "Estado",
        "TotalVentas",
        "Cuidados",
        'popularity',
    ];

    // Relación One to Many (Uno a muchos), un registro puede tener muchas imágenes
    public function imagenesPlantas()
    {
        return $this->hasMany('App\Models\Articulos\imgPlantas');
    }

}
