<?php

namespace App\Models\Articulos;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class imgPlantas extends Model
{
    use HasFactory;
    protected $table = 'img_plantas';

    protected $fillable = [
        'nombre',
        'formato',
        'planta_id',
    ];

    public function planta()
    {
    	return $this->belongsTo('App\Models\Articulos\Planta');
    }
}
