<?php

namespace App\Models\Articulos;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    use HasFactory;
    protected $table = 'productos';

    protected $fillable  = [
        "Id",
        "Nombre",
        "Cantidad",
        "Precio",
        "Imagenes",
        "Descripcion",
        "Estado",
        "TotalVentas",
        'popularity',
    ];

    // Relación One to Many (Uno a muchos), un registro puede tener muchas imágenes
    public function imagenesProductos()
    {
        return $this->hasMany('App\Models\Articulos\imgProductos');
    }

    # Accessor para mostrar una imagen destacada
    public function getFeaturedImageUrlAttribute(){
        $featuredImage = $this->imagenesProductos()->first();

        return $featuredImage->url;

        return '/images/products/default.png'; //modificar
    }

}
