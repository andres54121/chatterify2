<?php

namespace App\Models;

use App\Models\Shoppingcart\Cart;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasFactory, Notifiable, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'apellidoPa',
        'apellidoMa',
        'email',
        'password',
        'rol',
        'imagen',
        'id_direccion',
        'phone'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    // Definimos la relacion entre un usuario y un cart
    public function carts(){
        #Un usuario tendrá varios carts asociados
        #creamos lo analogo en el modelo Cart
        return $this->hasMany(Cart::class);

    }

    // Accessor para cart, devuelve el carrito activo
    public function getCartAttribute(){
        $cart = $this->carts()->where('status','Active')->first();

        if($cart)
            return $cart;

        # Creamos un nuevo carrito de compras activo para el usuario
        # debido a que para este caso, no tiene uno.
        $cart = new Cart();
        $cart->status = 'Active';
        $cart->user_id = $this->id;
        $cart->save();

        return $cart;

    }

    #Muestra todos los pedidos
    public function getOrderAttribute(){
        $order = $this->carts()->where('status', '!=','Active')->get();

        if ($order)
            return $order;

    }

    #Muestra los productos dentro de los pedidos cart->cartDetails
    public function getOrderDetailsAttribute(){
        $orderDetails = $this->carts()->where('status', '!=','Active')->first();

        return $orderDetails;

    }

    public function address()
    {
        return $this->hasOne(Address::class);
    }
}
