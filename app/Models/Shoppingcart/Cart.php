<?php

namespace App\Models\Shoppingcart;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    // Definimos la relacion entre un cart y sus detalles
    public function details(){
        #Un carrito tendrá muchos detalles  asociados
        return $this->hasMany('App\Models\Shoppingcart\CartDetail');
    }

    public function address()
    {
        return $this->hasOne(Address::class);
    }
}
