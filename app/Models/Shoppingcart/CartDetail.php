<?php

namespace App\Models\Shoppingcart;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CartDetail extends Model
{
    protected $fillable = [
        'id',
        'cart_id',
        'product_id',
        'planta_id',
        'quantity',
        'discount',
    ];
    //para que desde un detalle se pueda acceder al producto
    // CartDetail N                  1 Product
    // Un producto puede aparecer en varios carritos de compra
    // se pude asociar con varios detalles de carritos de compras
    public function product(){
        #Un detalle de un carrito de compras pertence a un producto
        return $this->belongsTo('App\Models\Articulos\Producto');
    }

    public function planta(){
        #Un detalle de un carrito de compras pertence a un producto
        return $this->belongsTo('App\Models\Articulos\Planta');
    }
}
