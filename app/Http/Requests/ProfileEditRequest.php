<?php

namespace App\Http\Requests;

use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Http\FormRequest;

class ProfileEditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = Auth::user()->id;
        return [
            'name' => 'required|max:25|regex:/^[\pL\s\-]+$/u',
            'apellidoPa'=> 'required|max:25|regex:/^[\pL\s\-]+$/u',
            'apellidoMa'=> 'required|max:25|regex:/^[\pL\s\-]+$/u',
            'phone'=> 'required|digits:10|unique:users,phone,' . $user, ///

            'CalleP'=> 'required|max:25|regex:/^[\pL\s\-0-9]+$/u',
            'entreCalles'=> 'required|max:50|regex:/^[\pL\s\-0-9.,]+$/u',
            'Colonia'=> 'required|max:30|regex:/^[\pL\s\-0-9]+$/u',
            'SMza'=> 'required|integer|not_in:0|digits_between:1,4',
            'Mza'=> 'required|integer|not_in:0|digits_between:1,3',
            'Lt'=> 'required|integer|not_in:0|digits_between:1,3',
            'Descripcion'=> 'required|regex:/^[\pL\s\-0-9.,]+$/u',
            'CP'=> 'required|digits:5',
        ];
    }
}
