<?php

namespace App\Http\Requests\Articulos;

use Illuminate\Foundation\Http\FormRequest;

class PlantaEditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $plantas = $this->route('plantas');
        return [
            'Nombre'=> 'required|max: 30|regex:/^[\pL\s\-]+$/u' ,
            'Tipo'=> 'required|in:Arbol,Arbustos,Trepadoras,Cactus,Crasas,Herbaceas,Hortícolas,Palmeras',
            'Cantidad'=> 'required|not_in:0|digits_between:1,1000000',
            'Precio'=> 'required|not_in:0|digits_between:1,1000000',
            // 'Imagen'=> 'required|image|mimes:jpg,jpeg,gif,png,',
            'img.*'=> 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'Descripcion'=> 'required|regex:/^[\pL\s\-0-9.,:;¿?!¡°]+$/u',
            "Cuidados_ID" => 'nullable', //=> 'required'
        ];
    }
}
