<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Articulos\Planta;
use App\Models\Articulos\Producto;
use App\Models\Articulos\imgPlantas;
use App\Models\Articulos\imgProductos;
use Illuminate\Support\Facades\Validator;

class CatalogoController extends Controller
{
    public function indexPlantas()
    {
        $plantas = Planta::where('Estado', '=', 'Habilitado')->paginate(12);
        // $imagenes = imgPlantas::where('planta_id', '=', $id)->get();
        return view('catalogo.plantas', compact('plantas'));
    }

    public function filtroPlantas(Request $request)
    {
        $vPl = Validator::make($request->all(), [
                'Tipo'=> 'required|in:Arbol,Arbustos,Trepadoras,Cactus,Crasas,Herbaceas,Hortícolas,Palmeras',
            ]);
        if ($vPl->fails())
            $this->indexPlantas();
        else{
            $plantas = Planta::where('Tipo', '=', $request->Tipo)->where('Estado', '=', 'Habilitado')->paginate(12);
            return view('catalogo.plantas', compact('plantas'));
        }
    }

    public function showPlanta($id)
    {
        $planta = Planta::findOrFail($id);
        $imagenes = imgPlantas::where('planta_id', '=', $id)->get();
        return view('catalogo.showPlanta', compact('imagenes'), ['planta'=> $planta]);
    }

    public function indexProductos()
    {
        $Productos = Producto::paginate(12);
        // $imagenes = imgProductos::where('planta_id', '=', $id)->get();
        return view('catalogo.herramientas', compact('Productos'));
    }

    public function showProducto($id)
    {
        $producto = Producto::findOrFail($id);
        $imagenes = imgProductos::where('producto_id', '=', $id)->get();
        return view('catalogo.showHerramienta', compact('imagenes'), ['Producto'=> $producto]);
    }
}
