<?php

namespace App\Http\Controllers\Paypal;

use PayPal\Api\Payer;
use PayPal\Api\Amount;
use App\Models\Address;
use PayPal\Api\Payment;
use PayPal\Api\Transaction;
use PayPal\Rest\ApiContext;
use Illuminate\Http\Request;
use PayPal\Api\RedirectUrls;
use PayPal\Api\PaymentExecution;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use PayPal\Auth\OAuthTokenCredential;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redirect;
use PayPal\Exception\PayPalConnectionException;

class PaymentController extends Controller
{
    private $apiContext;

    public function __construct()
    {
        $payPalConfig = Config::get('paypal');

        $this->apiContext = new ApiContext(
            new OAuthTokenCredential(
                $payPalConfig['client_id'],
                $payPalConfig['secret']
            )
        );

        $this->apiContext->setConfig($payPalConfig['settings']);
    }

    // ...

    public function payWithPayPal(Request $req)
    {
        # Recorremos cada producto del carrito para obtener la cantidad total
        $total = 0;
        foreach (auth()->user()->cart->details as $detail){
            if(!empty($detail->product->Precio))
                $total= $total + $detail->product->Precio * $detail->quantity;
            else
                $total= $total + $detail->planta->Precio * $detail->quantity;
        }

        $payer = new Payer();
        $payer->setPaymentMethod('paypal');

        $amount = new Amount();
        $amount->setTotal($total);
        $amount->setCurrency('MXN');

        $transaction = new Transaction();
        $transaction->setAmount($amount);
        $transaction->setDescription('Viveros Chatterify');

        $callbackUrl = url('/paypal/status');

        $redirectUrls = new RedirectUrls();
        $redirectUrls->setReturnUrl($callbackUrl)
            ->setCancelUrl($callbackUrl);

        $payment = new Payment();
        $payment->setIntent('sale')
            ->setPayer($payer)
            ->setTransactions(array($transaction))
            ->setRedirectUrls($redirectUrls);

        try {
            $payment->create($this->apiContext);
            return redirect()->away($payment->getApprovalLink());
        } catch (PayPalConnectionException $ex) {
            echo $ex->getData();
        }
    }

    public function payPalStatus(Request $request)
    {
        // Carrito
        $currentCart = auth()->user()->cart;
        $total = 0;
        foreach (auth()->user()->cart->details as $detail){
            if(!empty($detail->product->Precio))
                $total= $total + $detail->product->Precio * $detail->quantity;
            else
                $total= $total + $detail->planta->Precio * $detail->quantity;
        }
        // Paypal
        $paymentId = $request->input('paymentId');
        $payerId = $request->input('PayerID');
        $token = $request->input('token');

        if (!$paymentId || !$payerId || !$token) {
            $status = 'Lo sentimos! El pago a través de PayPal no se pudo realizar.';
            return redirect('/paypal/failed')->with(compact('status'));
        }

        $payment = Payment::get($paymentId, $this->apiContext);

        $execution = new PaymentExecution();
        $execution->setPayerId($payerId);

        /** Execute the payment **/
        $result = $payment->execute($execution, $this->apiContext);

        if ($result->getState() === 'approved') {
            // Guardamos la direccion del usuario
            $address = Address::find(Auth::user()->id_direccion);
            $newAddress = new Address();
            $newAddress->CalleP =  $address->CalleP;
            $newAddress->entreCalles =  $address->entreCalles;
            $newAddress->Colonia =  $address->Colonia;
            $newAddress->SMza =  $address->SMza;
            $newAddress->Mza =  $address->Mza;
            $newAddress->Lt =  $address->Lt;
            $newAddress->Descripcion =  $address->Descripcion;
            $newAddress->CP =  $address->CP;
            $newAddress->save();

            $DireccionID = $address->id;

            $currentCart->code          = $this->generateCode(8);
            $currentCart->total         = $total;
            $currentCart->order_date    = date('d-m-Y');
            $currentCart->status        = "Pendiente";
            $currentCart->address_id    = $DireccionID;
            $currentCart->save();

            $notification= "Gracias! El pago a través de PayPal se ha ralizado correctamente.";
            return redirect('/Shoppingcart')->with(compact('notification'));
        }

        $notification = 'Lo sentimos! El pago a través de PayPal no se pudo realizar.';
        return redirect('/Shoppingcart')->with(compact('notification'));
    }

    private function generateCode($len){

        $caracteres = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890"; //posibles caracteres a usar
        $cadena = ""; //variable para almacenar la cadena generada
        for($i=0;$i<$len;$i++)
            $cadena .= substr($caracteres,rand(0,strlen($caracteres)),1); /*Extraemos 1 caracter de los caracteres
        entre el rango 0 a Numero de letras que tiene la cadena */
        return $cadena;

    }
}
