<?php

namespace App\Http\Controllers\Articulos;

use Illuminate\Http\Request;
use App\Models\Articulos\Producto;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use App\Models\Articulos\imgProductos;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\Articulos\ProductoEditRequest;
use App\Http\Requests\Articulos\ProductoCreateRequest;

class ProductoController extends Controller
{
    const PERMISSIONS = [
        'create' => 'producto.create',
        'edit' => 'producto.edit',
        'destroy' => 'producto.destroy',
        // '' => '',
    ];

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role_or_permission:SuperAdmin|' . self::PERMISSIONS['create'])->only(['create', 'store']);
        $this->middleware('role_or_permission:SuperAdmin|' . self::PERMISSIONS['edit'])->only(['edit', 'update', 'eliminarimagen']);
        $this->middleware('role_or_permission:SuperAdmin|' . self::PERMISSIONS['destroy'])->only(['destroy']);
    }

    public function index()
    {
        if(Auth::user()->rol == 'usuario'){
            return redirect()->route("home");
        }
        $productos = Producto::paginate(10);
        return view("admin.Articulos.Producto.index", compact('productos'));
    }

    public function create()
    {
        return view('admin.Articulos.Producto.create');
    }

    public function store(ProductoCreateRequest $request)
    {
        // dd($request);
        // dd($_FILES);
        $producto = new Producto();
        $producto->Nombre = $request->Nombre;
        $producto->Cantidad = $request->Cantidad;
        $producto->Precio = $request->Precio;
        $producto->Descripcion = $request->Descripcion;
        $producto->Imagenes = date('dmyHi');
        $producto->save();

        // $image = $request->file('img');
        foreach($request->file('img') as $image)
        {
            $imagen = uniqid() . $image->getClientOriginalName();
            $formato = $image->getClientOriginalExtension();
            $image->move(public_path().'/uploads/producto/', $imagen);

            // Guardamos el nombre de la imagen en la tabla 'img_bicicletas'
            DB::table('img_productos')->insert(
                [
                    'nombre' => $imagen,
                    'formato' => $formato,
                    'producto_id' => $producto->id,
                    'created_at' => date("Y-m-d H:i:s"),
                    'updated_at' => date("Y-m-d H:i:s")
                ]
            );
        }

        return redirect()->route("productos.index")->with('success', 'Nuevo producto agregado correctamente');
    }

    public function show($id)
    {

    }

    public function edit($id)
    {
        $producto = Producto::findOrFail($id);
        $imagenes = imgProductos::where('producto_id', '=', $id)->get();
        return view('admin.Articulos.Producto.edit', compact('imagenes'), ['producto'=> $producto]);
    }

    public function update(ProductoEditRequest $request, $id)
    {
        $producto = Producto::findOrFail($id);
        $producto->update($request->all());
        // Actualizar imagenes
        $ci = $request->file('img');

        // Si la variable '$ci' no esta vacia, actualizamos el registro con las nuevas imágenes
        if(!empty($ci)){

            // Recibimos una o varias imágenes y las actualizamos
            foreach($request->file('img') as $image)
                {
                    $imagen = uniqid() . $image->getClientOriginalName();
                    $formato = $image->getClientOriginalExtension();
                    $image->move(public_path().'/uploads/producto/', $imagen);

                    // Actualizamos el nuevo nombre de la(s) imagen(es) en la tabla 'img_bicicletas'
                    DB::table('img_productos')->insert(
                        [
                            'nombre' => $imagen,
                            'formato' => $formato,
                            'producto_id' => $producto->id,
                            'created_at' => date("Y-m-d H:i:s"),
                            'updated_at' => date("Y-m-d H:i:s")
                        ]
                    );

                }

        }
        return redirect()->route("productos.index")->with('success', 'Producto actualizado correctamente');
    }

    public function destroy($id)
    {

        $producto = Producto::findOrFail($id);

        // Selecciono las imágenes a eliminar
        $imagen = DB::table('img_productos')->where('producto_id', '=', $id)->get();
        $imgfrm = $imagen->implode('nombre', ',');
        //dd($imgfrm);

        // Creamos una lista con los nombres de las imágenes separadas por coma
        $imagenes = explode(",", $imgfrm);

        // Recorremos la lista de imágenes separadas por coma
        foreach($imagenes as $image){

            // Elimino la(s) imagen(es) de la carpeta 'uploads'
            $dirimgs = public_path().'/uploads/producto/'.$image;

            // Verificamos si la(s) imagen(es) existe(n) y procedemos a eliminar
            if(File::exists($dirimgs)) {
                File::delete($dirimgs);
            }

        }
        $producto->imagenesProductos()->delete();
        $producto->delete();
        return redirect()->route("productos.index")->with('success', 'Eliminado correctamente');

    }

    // Eliminar imagen de un Registro
    public function eliminarimagen($id)
    {
        // Elimino la imagen de la carpeta 'uploads'
        $imagen = imgProductos::select('nombre')->where('id', '=', $id)->get();
        $imgfrm = $imagen->implode('nombre', ',');

        $producto = imgProductos::select('producto_id')->where('id', '=', $id)->get();
        $idP = $producto->implode('producto_id', ',');
        // Elimino la(s) imagen(es) de la carpeta 'uploads'
        $dirimgs = public_path().'/uploads/producto/'.$imgfrm;

        // Verificamos si la(s) imagen(es) existe(n) y procedemos a eliminar
        if(File::exists($dirimgs)) {
            File::delete($dirimgs);
        }

        imgProductos::destroy($id);

        // Redireccionamos con mensaje
        return Redirect::to('administracion/productos/'.$idP.'/edit');

    }

    // Cambiar el estado de la planta
    public function changeStatus($id)
    {
        $producto = Producto::findOrFail($id);
        if($producto->Estado == 'Habilitado'){
            $producto->Estado = 'Deshabilitado';
            $producto->save();
        }
        else if($producto->Estado == 'Deshabilitado'){
            $producto->Estado = 'Habilitado';
            $producto->save();
        }

        return redirect()->route("productos.index")->with('success', 'Estado del producto actualizado correctamente');
    }
}
