<?php

namespace App\Http\Controllers\Shoppingcart;

use App\Models\User;
use App\Models\Address;
use Illuminate\Http\Request;
use App\Models\Shoppingcart\Cart;
use App\Http\Controllers\Controller;
use App\Models\Articulos\imgPlantas;
use Illuminate\Support\Facades\Auth;
use App\Models\Articulos\imgProductos;

class CartController extends Controller
{
    const PERMISSIONS = [
        'edit' => 'pedido.status'
    ];

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role_or_permission:SuperAdmin|' . self::PERMISSIONS['edit'])->only(['updateStatus']);
    }

    public function indexUser()
    {
        if(Auth::user()->rol == 'Admin'){
            $users = User::all();
            return view('admin.ventas', compact('users'));
        }
        else{
            return redirect()->route("home");
        }
    }

    public function update(Request $req){
        $currentCart                = auth()->user()->cart;
        # Recorremos cada producto del carrito para obtener la cantidad total
        $total = 0;
        foreach (auth()->user()->cart->details as $detail){
            if(!empty($detail->product->Precio))
                $total= $total + $detail->product->Precio * $detail->quantity;
            else
                $total= $total + $detail->planta->Precio * $detail->quantity;
        }
        // Guardamos la direccion del usuario
        $address = Address::find(Auth::user()->id_direccion);
        // dd($address);
        $newAddress = new Address();
        $newAddress->CalleP =  $address->CalleP;
        $newAddress->entreCalles =  $address->entreCalles;
        $newAddress->Colonia =  $address->Colonia;
        $newAddress->SMza =  $address->SMza;
        $newAddress->Mza =  $address->Mza;
        $newAddress->Lt =  $address->Lt;
        $newAddress->Descripcion =  $address->Descripcion;
        $newAddress->CP =  $address->CP;
        $newAddress->save();

        $DireccionID = $address->id;

        $currentCart->code          = $this->generateCode(8);
        $currentCart->total         = $total;
        $currentCart->order_date    = date('d-m-Y');
        $currentCart->status        = "Pendiente";
        $currentCart->address_id         = $DireccionID;
        $currentCart->save();

        $notification= "Tu pedido se ha registrado correctamente.";
        return back()->with(compact('notification'));
    }

    public function updateStatus(Request $req, $id){
        // dd($req, $id);
        $order =  Cart::find($id);

        # validar
        $order->status = $req->status;

        #Al cambiar a finalizado, la fecha de llegada se coloca como actual
        if($req->status == "Finalizado")
            $order->arrived_date = date('d-m-Y');
        $order->save();

        $notification           = "¡El status del producto ha cambiado!";
        return back()->with(compact('notification'));
    }

    public function destroy(Request $req){

        #$order = auth()->user()->cart;
        #$order = Cart::find($req->order_id);
        $order = auth()->user()->orderDetails;

        #if($cartDetail->cart_id == auth()->user()->cart->id)
        $order->delete();

        $notification = "¡El pedido seleccionado se ha eliminado correctamente!.";
        return back()->with(compact('notification'));

    }

    private function generateCode($len){

        $caracteres = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890"; //posibles caracteres a usar
        $cadena = ""; //variable para almacenar la cadena generada
        for($i=0;$i<$len;$i++)
            $cadena .= substr($caracteres,rand(0,strlen($caracteres)),1); /*Extraemos 1 caracter de los caracteres
        entre el rango 0 a Numero de letras que tiene la cadena */
        return $cadena;

    }
}
