<?php

namespace App\Http\Controllers\Shoppingcart;

use Illuminate\Http\Request;
use App\Models\Articulos\Planta;
use App\Models\Articulos\Producto;
use App\Http\Controllers\Controller;
use App\Models\Shoppingcart\Cart;
use App\Models\Shoppingcart\CartDetail;
use Illuminate\Support\Facades\Validator;

class CartDetailController extends Controller
{
    public function store(Request $request){

        $current_cart_id = auth()->user()->cart->id;

        $cartDetail             = new CartDetail();
        # ¿A que carrito pertenece ?
        # definimos un campo cart en user que devuelva la informacion del carrito de compras
        # activo para ese usuario que esta en la sesion.
        # Definimos un accessor en el modelo User para poder obtener su id
        $cartDetail->cart_id    = $current_cart_id;
        if(empty( $request->product_id )){
            $cartDetail2       = CartDetail::where('cart_id', '=', $current_cart_id)->where('planta_id', '=', $request->planta_id)->first();
            if(!empty($cartDetail2)){
                $quantity = $request->quantity + $cartDetail2->quantity;
                $this->destroyX($cartDetail2);

                $cartDetail2->quantity = $quantity;
                $newCartDetail = new CartDetail();
                $newCartDetail->cart_id =  $cartDetail2->cart_id;
                $newCartDetail->product_id =  $cartDetail2->product_id;
                $newCartDetail->planta_id =  $cartDetail2->planta_id;
                $newCartDetail->quantity =  $quantity;
                $newCartDetail->discount =  $cartDetail2->discount;
                $newCartDetail->save();
                $notification = "El producto se agregó exitosamente";
                return back()->with(compact('notification'));
            }
            else{
                $cartDetail->planta_id = $request->planta_id;
                $planta                = Planta::find($request->planta_id);
                $vPl = Validator::make($request->all(), [
                    'quantity'=> 'not_in:0|numeric|min:1|max:' . $planta->Cantidad
                ]);
                if ($vPl->fails())
                    return redirect()->back()->with('error', 'Cantidad no valida');

                $planta->popularity    = $planta->popularity + 1;
                $planta->save();
            }
        }
        else{
            $cartDetail2       = CartDetail::where('cart_id', '=', $current_cart_id)->where('product_id', '=', $request->product_id)->first();
            if(!empty($cartDetail2)){
                $quantity = $request->quantity + $cartDetail2->quantity;
                $this->destroyX($cartDetail2);

                $cartDetail2->quantity = $quantity;
                $newCartDetail = new CartDetail();
                $newCartDetail->cart_id =  $cartDetail2->cart_id;
                $newCartDetail->product_id =  $cartDetail2->product_id;
                $newCartDetail->planta_id =  $cartDetail2->planta_id;
                $newCartDetail->quantity =  $quantity;
                $newCartDetail->discount =  $cartDetail2->discount;
                $newCartDetail->save();
                $notification = "El producto se agregó exitosamente";
                return back()->with(compact('notification'));
            }

            else{
                $cartDetail->product_id = $request->product_id;
                $product                = Producto::find($request->product_id);
                $vPl = Validator::make($request->all(), [
                    'quantity'=> 'not_in:0|numeric|min:1|max:' . $product->Cantidad
                ]);
                if ($vPl->fails())
                    return redirect()->back()->with('error', 'Cantidad no valida');
                $product->popularity    = $product->popularity + 1;
                $product->save();
            }
        }
        # Buscamos el producto seleccionado para aumentar su nivel de popularidadad:
        # Se asume que cuando un usuario agrega un producto al carrito es porque le gustó
        # y tiene la intencion de comprarlo...

        $cartDetail->quantity   = $request->quantity;
        $cartDetail->save();

        $notification = "El producto se agregó exitosamente";
        return back()->with(compact('notification'));
    }

    public function destroy(Request $request){

        $cartDetail             = CartDetail::find($request->cart_detail_id);
        # Se debe comprobar si el id del carrito de compras que se quiere eliminar
        # es de el usuario autenticado.. de lo contrario es una vulnerabilidad
        if($cartDetail->cart_id == auth()->user()->cart->id)
            $cartDetail->delete();

        $notification = "¡El producto se ha eliminado del carrito correctamente!.";
        return back()->with(compact('notification'));
    }

    public function getProduct(){
        #Un producto puede aparecer en muchos carritos de compras
        return $this->belongsTo(Product::class);
    }

    public function getPlanta(){
        #Una planta puede aparecer en muchos carritos de compras
        return $this->belongsTo(Planta::class);
    }

    private function destroyX($cartDetailID){
        $cartDetail             = CartDetail::find($cartDetailID->id);
        // dd($cartDetail);
        # Se debe comprobar si el id del carrito de compras que se quiere eliminar
        # es de el usuario autenticado.. de lo contrario es una vulnerabilidad
        if($cartDetail->cart_id == auth()->user()->cart->id)
            $cartDetail->delete();
    }
}
