<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Permission;
use App\Http\Requests\RolCreateRequest;

class RoleController extends Controller
{

    public function __construct()
    {
        $this->middleware(['role:SuperAdmin']);
    }

    public function index()
    {
        $roles = Role::orderBy('name')->paginate(10);
        return view('admin.role.index', compact('roles'));
    }

    public function create()
    {
        $permissions = Permission::all();
        return view('admin.role.create', compact('permissions'));
    }

    public function store(RolCreateRequest $request)
    {

        $rol = new Role($request->all());
        $rol->save();
        $rol->permissions()->sync($request->permission);
        return redirect()->route('roles.index')->with('success', 'Rol creado correctamente');
    }

    public function show($id)
    {
        $rol = Role::findOrFail($id);
        return view('admin.role.show',[ 'rol' => $rol->load('permissions', 'users')]);
    }

    public function edit($id)
    {
        $rol = Role::findOrFail($id);
        $permissions = Permission::all();
        return view('admin.role.edit', compact('rol', 'permissions'));
    }

    public function update(Request $request, $id)
    {
        $rule = [
            'name' => 'required|max:10|unique:roles,name,' . $id,
            'description'=> 'required|max:50',
        ];
        $this->validate($request, $rule);

        $rol = Role::findOrFail($id);
        $rol->update($request->all());
        $rol->permissions()->sync($request->permission);
        return redirect()->route("roles.index")->with('success', 'Rol actualizado correctamente');
    }

    public function destroy($id)
    {
        $rol = Role::findOrFail($id);
        $rol->delete();
        return redirect()->route("roles.index")->with('success', 'Rol eliminado correctamente');
    }
}
