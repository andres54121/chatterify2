<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Address;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\passwordRequest;
use App\Http\Requests\ProfileEditRequest;

class ProfileController extends Controller
{

    public function index()
    {
        $address = Address::find(Auth::user()->id_direccion);
        return view('users.profile', compact('address'));
    }

    public function update(ProfileEditRequest $request)
    {
        $id = Auth::user()->id;
        $user = User::findOrFail($id);
        $data = $request->only('name', 'apellidoPa', 'apellidoMa', 'phone');

        if(empty(Auth::user()->id_direccion )){
            $address = Address::create($request->except('name', 'apellidoPa', 'apellidoMa', 'phone'));
            $data['id_direccion']  = $address->id;
        }
        else{
            $address = Address::findOrFail(Auth::user()->id_direccion);
            $dataAddress =  $request->except('name', 'apellidoPa', 'apellidoMa', 'phone');
            $address->update($dataAddress);
        }

        $user->update($data);

        return redirect()->route("profile")->with('notification', 'Usuario actualizado correctamente');
    }

    public function changePassword(passwordRequest $request)
    {
        $id = Auth::user()->id;
        $user = User::findOrFail($id);
        $data['password'] = bcrypt($request->password);

        $user->update($data);
        return redirect()->route("profile")->with('notification', 'Contraseña actualizada correctamente');
    }
}
