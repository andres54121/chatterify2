<?php

namespace App;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Validator as LaravelValidator;

class validator extends LaravelValidator
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function validateCurrentPassword($attribute, $value, $parameters)
    {
        return Hash::check($value, Auth::user()->password);
    }

    public function authorize()
    {
        return true;
    }

}
