<?php

namespace App\Providers;

use App\Validator as Validacion;
use Illuminate\Support\Facades\Validator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Paginator::useBootstrap();

        Validator::resolver(function($translator, $data, $rules, $messages)
        {
            return new Validacion($translator, $data, $rules, $messages);
        });
    }
}
