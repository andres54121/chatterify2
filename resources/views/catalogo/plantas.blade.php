@extends('layouts.Shoppingcart')

@section('title', 'Plantas')
    <!-- si se quiere agregar una clase que esta en el tag body: -->
@section('body-class', 'landing-page sidebar-collapse')
    <!-- estilos solo para esta pagina que no se aplicaron-->
@section('styless')
    <style>
        .row {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            flex-wrap: wrap;
        }

        .row>[class*='col-'] {
            display: flex;
            flex-direction: column;
        }

    </style>
@endsection

@section('content')

    <!--jumbotron-->
    <div class="page-header header-filter" data-parallax="true"
        style="background-image: url({{ asset('https://images.pexels.com/photos/4503732/pexels-photo-4503732.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260') }})">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h1 class="title">Nuestras Plantas.</h1>
                    <h4>La mejor calidad de plantas del mercado solo para ti</h4>
                    <br>

                </div>
            </div>
        </div>
    </div>
    <!-- end jumbotron-->

    <!--principal content, similar al landing page-->
    <div class="main main-raised">
        <div class="container">

            <!--End name-->
            @if (session('notification'))
                <div class="alert alert-success text-center">
                    {{ session('notification') }}
                </div>
            @endif

            @if (session('error'))
                <div class="alert alert-warning text-center">
                    {{ session('error') }}
                </div>
            @endif
            {{--  --}}
            {{-- Filtro --}}
            <div class="row mt-5">

                <div class="col-6 ">

                </div>

                <div class="col-6">

                    <form action="{{ route('plantas') }}" method="post" class="form-horizontal mt-5 mr-5 form-inline float-right "
                        enctype="multipart/form-data">
                    @csrf
                    <div class="form-group float-right ">
                        <label class="mr-3">Tipo de Planta</label>
                        <select class="form-control w-30" name="Tipo">
                            <option value="Arbol">Árbol</option>
                            <option value="Arbustos">Arbustos</option>
                            <option value="Trepadoras">Trepadoras</option>
                            <option value="Cactus">Cactus</option>
                            <option value="Crasas">Crasas</option>
                            <option value="Herbaceas">Herbáceas</option>
                            <option value="Hortícolas">Hortícolas</option>
                            <option value="Palmeras">Palmeras</option>
                        </select>
                        @if ($errors->has('Tipo'))
                            <span class="error text-danger" for="input-Tipo">{{ $errors->first('Tipo') }}</span>
                        @endif
                    </div>
                    <button type="submit" class="btn btn-primary ml-4">Filtrar</button>
                    </form>
                </div>
            </div>
            <!--End profile -->
            <div class="album  py-5 ">
                <div class="container">
                    <div class="row">
                        @if ($plantas->count()>0)
                            @foreach ($plantas as $planta)
                                <div class="col-md-4">
                                    <div class="card mb-4 box-shadow">
                                        <!-- Plantas -->
                                        @php
                                            $imgPlantas = App\Models\Articulos\imgPlantas::where('planta_id', '=', $planta->id)->get();
                                            if (empty($imgPlantas[0]))
                                                $nombre = 'Image.png';
                                            else
                                                $nombre = $imgPlantas[0]->nombre;
                                        @endphp
                                        <img class="card-img-top"
                                            data-src="holder.js/100px225?theme=thumb&amp;bg=55595c&amp;fg=eceeef&amp;text=Thumbnail"
                                            alt="Thumbnail [100%x225]" style="height: 200px; width: 100%; display: block;"
                                            src="../../../uploads/planta/{{ $nombre }}"
                                            data-holder-rendered="true">
                                        <div class="card-body text-center">
                                            <h4 class="card-title"> <a href="#">{{ $planta->Nombre }}</a>
                                                <p class="card-description">Tipo: {{ $planta->Tipo }}</p>
                                                <small class="card-description text-muted">Precio</small>
                                            </h4>
                                            <div class="">
                                                <h4 class="card-description text-primary">
                                                    ${{ number_format($planta->Precio, 2) }}<a href="#"></a></h4>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="ml-3">
                                                @include('layouts.Shoppingcart.modalAddToCartPlanta')
                                                <a href="{{ url('/catalogo/plantas/' . $planta->id) }}" type="button"
                                                    class="btn btn-outline-primary  btn-round ">
                                                    <i class="material-icons">add</i> Ver
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach

                        @else
                        <div class="col-md-12">
                            <div class="info">
                                <div class="icon icon-info text-center">
                                    <i class="material-icons">info</i>
                                </div>
                                <div class="text-center">
                                    <h2>Muy Pronto!!!</h2>
                                </div>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>

            <div class="card-footter ml-auto">
                {{ $plantas->links() }}
            </div>
            <br>
        </div>
    </div>
    <!--end principal content-->
@endsection

@section('scripts')
    <script src="{{ asset('/js/typeahead.bundle.min.js') }}"> </script>
    <script>
        $(function() {
            // Inicializamos el motor de busqueda que realmente hace las sugerencias
            // bundle: una agrupacion de ambos
            var products = new Bloodhound({
                datumTokenizer: Bloodhound.tokenizers.whitespace,
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                prefetch: '{{ url('/products/json') }}'
            });

            // Inicializamos typeahead sobre el input de busqueda
            $('#search').typeahead({
                //propiedades
                hint: true,
                highlight: true,
                minLength: 1
            }, {
                // dataset
                name: 'products',
                source: products
            });

        });

    </script>
@endsection
