@extends('layouts.Shoppingcart')

@section('title', 'Herramientas')
    <!-- si se quiere agregar una clase que esta en el tag body: -->
@section('body-class', 'profile-page sidebar-collapse')
    <!-- estilos solo para esta pagina que no se aplicaron-->
@section('styless')
    <style>
        .row {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            flex-wrap: wrap;
        }

        .row>[class*='col-'] {
            display: flex;
            flex-direction: column;
        }

    </style>
@endsection

@section('content')

    <!--jumbotron-->
    <div class="page-header"
        style="background-image: url({{ asset('https://images.pexels.com/photos/4503267/pexels-photo-4503267.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260') }})">
    </div>
    <!-- end jumbotron-->


    <!--principal content, similar al landing page-->
    <div class="main main-raised">
        <div class="container profile-content">
            <br>
            <h2 class="title text-center"> {{ $Producto->Nombre }} </h2><br>

            <!--End name-->
            @if(session('notification'))
            <div class="alert alert-success text-center">
                {{ session('notification') }}
            </div>
            @endif

            @if(session('error'))
            <div class="alert alert-warning text-center">
                {{ session('error') }}
            </div>
            @endif
        </div>
        <!--End profile -->
        </div>
    </div>
    <div class="container">
        <div class="section text-center">
            <link rel='stylesheet' href='https://sachinchoolur.github.io/lightslider/dist/css/lightslider.css'>
            <div class="container-fluid mt-2 mb-3">
                <div class="row no-gutters">
                    <div class="col-md-5 pr-2">
                        <div class="card">
                            <div class="demo">
                                <ul id="lightSlider">
                                    @foreach ($imagenes as $imagen)
                                        <li data-thumb="../../../uploads/producto/{{ $imagen->nombre }}"> <img
                                                src="../../../uploads/producto/{{ $imagen->nombre }}" /> </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-7">
                        <div class="card">

                            <div class="about"> <span class="font-weight-bold"></span>
                                <h4 class="font-weight-bold">${{ number_format($Producto->Precio, 2 ) }}</h4>
                            </div>
                            {{-- Botones --}}
                            <div class="text-center">
                                @include('layouts.Shoppingcart.modalAddToCart')
                                <a href="{{ url('/catalogo/herramientas') }}" type="button" class="btn btn-btn-outline-light btn-round">
                                    <i class="material-icons">arrow_back</i> Regresar
                                </a>
                              </div>
                            {{--  --}}
                            <hr>
                            <div class="product-description">

                                <div class="mt-2"> <span class="font-weight-bold">Description</span>
                                    <p>{{$Producto->Descripcion}}</p>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <script src='https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js'></script>
            <script src='https://sachinchoolur.github.io/lightslider/dist/js/lightslider.js'></script>
            <script>
                $('#lightSlider').lightSlider({
                    gallery: true,
                    item: 1,
                    loop: true,
                    slideMargin: 0,
                    thumbItem: 9
                });

            </script>
        </div>
    </div>
    <!--end principal content-->
@endsection

@section('scripts')
    <script src="{{ asset('/js/typeahead.bundle.min.js') }}"> </script>
    <script>
        $(function() {
            // Inicializamos el motor de busqueda que realmente hace las sugerencias
            // bundle: una agrupacion de ambos
            var products = new Bloodhound({
                datumTokenizer: Bloodhound.tokenizers.whitespace,
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                prefetch: '{{ url(' / products / json ') }}'
            });

            // Inicializamos typeahead sobre el input de busqueda
            $('#search').typeahead({
                //propiedades
                hint: true,
                highlight: true,
                minLength: 1
            }, {
                // dataset
                name: 'products',
                source: products
            });

        });

    </script>
@endsection
