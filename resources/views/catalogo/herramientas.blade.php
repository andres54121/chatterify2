@extends('layouts.Shoppingcart')

@section('title','Herramientas')
<!-- si se quiere agregar una clase que esta en el tag body: -->
@section('body-class', 'landing-page sidebar-collapse')
<!-- estilos solo para esta pagina que no se aplicaron-->
@section('styless')
<style>
    .row {
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        flex-wrap: wrap;
    }

    .row>[class*='col-'] {
        display: flex;
        flex-direction: column;
    }

</style>
@endsection

@section('content')

<!--jumbotron-->
<div class="page-header header-filter" data-parallax="true" style="background-image: url({{ asset('https://images.pexels.com/photos/4503267/pexels-photo-4503267.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260') }})">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h1 class="title">Nuestros Productos.</h1>
                <h4>Tenemos los mejores productos solo para ti</h4>
                <br>

            </div>
        </div>
    </div>
</div>
<!-- end jumbotron-->

<!--principal content, similar al landing page-->
<div class="main main-raised">
    <div class="container">
        <div class="section ">
            <!--End name-->
            @if(session('notification'))
            <div class="alert alert-success text-center">
                {{ session('notification') }}
            </div>
            @endif

            @if(session('error'))
            <div class="alert alert-warning text-center">
                {{ session('error') }}
            </div>
            @endif
            {{-- Filtro --}}

            {{--  --}}
        </div>
        <!--End profile -->
        <div class="album  py-5 ">
            <div class="container">
                <div class="row">
                    @foreach ($Productos as $Producto)
                    <div class="col-md-4">
                        <div class="card mb-4 box-shadow">

                            <!-- Productos -->
                            @php
                                $imgProductos = App\Models\Articulos\imgProductos::where('Producto_id', '=', $Producto->id)->get();
                                if (empty($imgProductos[0]))
                                    $nombre = 'Image.png';
                                else
                                    $nombre = $imgProductos[0]->nombre;
                            @endphp
                            <img class="card-img-top" data-src="holder.js/100px225?theme=thumb&amp;bg=55595c&amp;fg=eceeef&amp;text=Thumbnail" alt="Thumbnail [100%x225]" style="height: 200px; width: 100%; display: block;" src="../../../uploads/Producto/{{ $nombre }}" data-holder-rendered="true">
                            <div class="card-body text-center">
                                <h4 class="card-title"> <a href="#">{{$Producto->Nombre}}</a>
                                    <p class="card-description"></p>
                                    <small class="card-description text-muted">Precio</small>
                                </h4>
                                <div class="">
                                    <h4 class="card-description text-primary">${{ number_format($Producto->Precio, 2 ) }}<a href="#"></a></h4>
                                </div>
                            </div>
                            <div class="row">
                                <div class="ml-3">
                                    @include('layouts.Shoppingcart.modalAddToCart')
                                    <a href="{{ url('/catalogo/herramientas/'. $Producto->id) }}" type="button" class="btn btn-outline-primary  btn-round">
                                        <i class="material-icons">add</i> Ver
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>

        <div class="card-footter ml-auto">
            {{ $Productos->links() }}
        </div>
        <br>
    </div>
</div>
<!--end principal content-->
@endsection

@section('scripts')
<script src="{{ asset('/js/typeahead.bundle.min.js') }}"> </script>
<script>
    $(function() {
        // Inicializamos el motor de busqueda que realmente hace las sugerencias
        // bundle: una agrupacion de ambos
        var products = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.whitespace
            , queryTokenizer: Bloodhound.tokenizers.whitespace
            , prefetch: '{{ url("/products/json") }}'
        });

        // Inicializamos typeahead sobre el input de busqueda
        $('#search').typeahead({
            //propiedades
            hint: true
            , highlight: true
            , minLength: 1
        }, {
            // dataset
            name: 'products'
            , source: products
        });

    });

</script>
@endsection
