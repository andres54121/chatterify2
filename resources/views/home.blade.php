@extends('layouts.Shoppingcart')

@section('title','Bienvenido a Chatterify')
<!-- si se quiere agregar una clase que esta en el tag body: -->
@section('body-class', 'landing-page sidebar-collapse')
<!-- estilos solo para esta pagina que no se aplicaron-->
@section('styless')
<style>
    .row {
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        flex-wrap: wrap;
    }

    .row>[class*='col-'] {
        display: flex;
        flex-direction: column;
    }

</style>
@endsection

@section('content')

<!--jumbotron-->
<div class="page-header header-filter" data-parallax="true" style="background-image: url({{ asset('img/lily.jpg') }})">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div id="carouselExampleFade" class="carousel slide carousel-fade" data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img src="https://mdbootstrap.com/img/new/slides/041.jpg" class="d-block w-100" alt="..." />
                        </div>
                        <div class="carousel-item">
                            <img src="https://mdbootstrap.com/img/new/slides/042.jpg" class="d-block w-100" alt="..." />
                        </div>
                        <div class="carousel-item">
                            <img src="https://mdbootstrap.com/img/new/slides/043.jpg" class="d-block w-100" alt="..." />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end jumbotron-->


<!--principal content, similar al landing page-->
<div class="main main-raised">
    <!-- Products section -->
    <div class="section text-center">
        <h2 class="title">Plantas destacados </h2>
        <br>
        @php $plantas = App\Models\Articulos\Planta::orderBy('popularity', 'desc')->take(3)->get(); @endphp
        <div class="container">
            <div class="album py-5  ">
                <div class="container">
                    <div class="row">
                        @foreach ($plantas as $planta)
                        <div class="col-md-4">
                            <div class="card mb-4 box-shadow">
                                @php
                                    $imgPlantas = App\Models\Articulos\imgPlantas::where('planta_id', '=', $planta->id)->get();
                                    if (empty($imgPlantas[0]))
                                        $nombre = 'Image.png';
                                    else
                                        $nombre = $imgPlantas[0]->nombre;
                                @endphp
                                <img class="card-img-top"  src="../../../uploads/planta/{{ $nombre }}" data-holder-rendered="true">
                                <div class="card-body text-center">
                                    <h4 class="card-title"> <a href="#">{{$planta->Nombre}}</a>
                                        <p class="card-description">Tipo: {{$planta->Tipo}}</p>
                                        <small class="card-description text-muted">Precio</small>
                                    </h4>
                                    <div class="">
                                        <h4 class="card-description text-primary">${{ number_format($planta->Precio, 2 ) }}<a href="#"></a></h4>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="ml-3">
                                        @include('layouts.Shoppingcart.modalAddToCartPlanta')
                                        <a href="{{ url('/catalogo/plantas/'. $planta->id) }}" type="button" class="btn btn-outline-primary  btn-round">
                                            <i class="material-icons">add</i> Ver
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

        <div class="text-center mt-5">
            <a href="{{ url('/catalogo/plantas') }}" type="button" class="btn btn-outline-primary  btn-round">
                <i class="material-icons">add</i> Ver más Plantas
            </a>
        </div>
    </div>
    <!-- Plantas section -->
    <div class="section text-center">
        <h2 class="title">Productos de jardineria destacados </h2>
        <br>
        @php $Productos = App\Models\Articulos\Producto::orderBy('popularity', 'desc')->take(3)->get(); @endphp
        <div class="container">
            <div class="album py-5  ">
                <div class="container">
                    <div class="row">
                        @foreach ($Productos as $Producto)
                        <div class="col-md-4">
                            <div class="card mb-4 box-shadow">

                                <!-- Productos -->
                                @php
                                    $imgProductos = App\Models\Articulos\imgProductos::where('Producto_id', '=', $Producto->id)->get();
                                    if (empty($imgProductos[0]))
                                        $nombre = 'Image.png';
                                    else
                                        $nombre = $imgProductos[0]->nombre;
                                @endphp
                                <img class="card-img-top" alt="Thumbnail [100%x225]" style="height: 200px; width: 100%; display: block;" src="../../../uploads/Producto/{{ $nombre }}" data-holder-rendered="true">
                                <div class="card-body text-center">
                                    <h4 class="card-title"> <a href="#">{{$Producto->Nombre}}</a>
                                        <p class="card-description"></p>
                                        <small class="card-description text-muted">Precio</small>
                                    </h4>
                                    <div class="">
                                        <h4 class="card-description text-primary">${{ number_format($Producto->Precio, 2 ) }}<a href="#"></a></h4>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="ml-3">
                                        @include('layouts.Shoppingcart.modalAddToCart')
                                        <a href="{{ url('/catalogo/herramientas/'. $Producto->id) }}" type="button" class="btn btn-outline-primary  btn-round">
                                            <i class="material-icons">add</i> Ver
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

        <!--End button -->

        <div class="text-center mt-5">
            <a href="{{ url('/catalogo/herramientas') }}" type="button" class="btn btn-outline-primary  btn-round">
                <i class="material-icons">add</i> Ver más Herramientas
            </a>
        </div>
    </div>

    <div class="container mb-5">

        <div class="section section-contacts  ">
            <div class="row">
                <div class="col-md-8 ml-auto mr-auto">
                    <h2 class="text-center title">Trabajamos para ti</h2>
                    <h4 class="text-center description">Si aún no te registras, puedes enviarnos tus dudas y te responderemos a la brevedad.</h4>
                    <form class="contact-form" method="post" action="{{ url('/message/new') }}">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">¿Cuál es tu nombre?</label>
                                    <input type="text" name="name_user" class="form-control">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Tu Correo Electronico</label>
                                    <input type="email" name="email" class="form-control">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="exampleMessage" class="bmd-label-floating">Plantea tus dudas</label>
                            <textarea type="text" name="message" class="form-control" rows="4" id="exampleMessage"></textarea>
                        </div>
                        <div class="row">
                            <div class="col-md-4 ml-auto mr-auto text-center">
                                <button class="btn btn-primary btn-raised">
                                    Enviar Consulta
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>


    </div>
</div>
<!--end principal content-->
@endsection

@section('scripts')
<script src="{{ asset('/js/typeahead.bundle.min.js') }}"> </script>
<script>
    $(function() {
        // Inicializamos el motor de busqueda que realmente hace las sugerencias
        // bundle: una agrupacion de ambos
        var products = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.whitespace
            , queryTokenizer: Bloodhound.tokenizers.whitespace
            , prefetch: '{{ url("/products/json") }}'
        });

        // Inicializamos typeahead sobre el input de busqueda
        $('#search').typeahead({
            //propiedades
            hint: true
            , highlight: true
            , minLength: 1
        }, {
            // dataset
            name: 'products'
            , source: products
        });

    });

</script>
@endsection
