@extends('layouts.main', ['activePage' => 'users', 'titlePage' => 'Editar Usuario'])
@section('content')

<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <form action="{{ route('usuario.update', $user->id) }}" method="post" class="form-horizontal">

                    @csrf
                    @method('PUT')
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-tittle">Usuario</h4>
                            <p class="card-category">Editar Datos</p>
                        </div>

                        <div class="card-body">
                            <div class="row">
                                <label for="name" class="col-sm-2 col-form-label">Nombre:</label>
                                <div class="col-sm-7">
                                    <input type="text" name="name" class="form-control" value="{{ old('name', $user->name)}}" placeholder="Nombre" autofocus required>
                                    @if ($errors->has('name'))
                                        <span class="error text-danger" for="input-name">{{ $errors->first('name') }}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="row">
                                <label for="apellidoPa" class="col-sm-2 col-form-label">Apellido Paterno:</label>
                                <div class="col-sm-7">
                                    <input type="text" name="apellidoPa" class="form-control" value="{{ old('apellidoPa', $user->apellidoPa)}}" placeholder="Apellido Paterno" required>
                                    @if ($errors->has('apellidoPa'))
                                        <span class="error text-danger" for="input-apellidoPa">{{ $errors->first('apellidoPa') }}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="row">
                                <label for="apellidoMa" class="col-sm-2 col-form-label">Apellido Materno:</label>
                                <div class="col-sm-7">
                                    <input type="text" name="apellidoMa" class="form-control" value="{{ old('apellidoMa', $user->apellidoMa)}}" placeholder="Apellido Materno" required>
                                    @if ($errors->has('apellidoMa'))
                                        <span class="error text-danger" for="input-apellidoMa">{{ $errors->first('apellidoMa') }}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="row">
                                <label for="email" class="col-sm-2 col-form-label">Correo:</label>
                                <div class="col-sm-7">
                                    <input type="email" name="email" class="form-control" value="{{ old('email',$user->email)}}" required>
                                    @if ($errors->has('email'))
                                        <span class="error text-danger" for="input-email">{{ $errors->first('email') }}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="row">
                                <label for="password" class="col-sm-2 col-form-label">Contraseña:</label>
                                <div class="col-sm-7">
                                    <input type="password" name="password" class="form-control" placeholder="Ingrese la contraseña solo en caso de modificarlo">
                                    @if ($errors->has('password'))
                                        <span class="error text-danger" for="input-password">{{ $errors->first('password') }}</span>
                                    @endif
                                </div>
                            </div>

                            <input  style="display: none" type="text" name="rol" class="form-control" value="Admin">
                        </div>
                    </div>

                    {{-- Listado de Roles --}}
                    <div class="card">
                        <div class="card-header card-header-success">
                            <h4 class="card-tittle">Lista de Roles</h4>
                            <p class="card-category">Seleccione el rol que tendra este usuario</p>
                        </div>

                        <div class="card-body">
                            <div class="row input-group">
                                @foreach ($roles as $role)
                                    @if ($role->description != 'Cliente')
                                        <div class="col-md-4">
                                            {!! Form::radio(
                                                "role",
                                                $role->id,
                                                $user->hasRole($role->id)
                                            ) !!}
                                            <span>{{ $role->name }}</span>
                                        </div>
                                    @endif
                                @endforeach
                            </div>
                        </div>

                        <div class="card-footer ml-auto mr-auto">
                            <button type="submit" class="btn btn-primary">Guardar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
