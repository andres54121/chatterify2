@extends('layouts.main', ['activePage' => 'roles', 'titlePage' => 'Lista de Roles'])
@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header card-header-primary">
                                <h4 class="card-tittle">Rol: {{$rol->name}}</h4>
                                <p class="card-category">{{$rol->description}}</p>
                            </div>

                            <div class="card-body">
                                @if ($rol->name != 'SuperAdmin')
                                <div class="col-12 text-right">
                                    <a href="{{route("roles.edit", $rol->id)}}" class="btn btn-sm btn-warning">Editar Rol</a>
                                </div>
                                @endif
                            </div>
                        </div>
                        {{-- Listado de Permisos --}}

                        @if ($rol->name != 'SuperAdmin')
                        <div class="card">
                            <div class="card-header card-header-success">
                                <h4 class="card-tittle">Permisos Asignados</h4>
                                <p class="card-category">Permisos con los que cuenta este Rol</p>
                            </div>

                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead class="text-primary">
                                            <th>Nombre</th>
                                            <th>Descripcion</th>
                                            <th>Fecha de Creacion</th>
                                        </thead>
                                        <tbody>
                                            @foreach ($rol->permissions as $permission)
                                                <tr>
                                                    <td>{{$permission->name}}</td>
                                                    <td>{{$permission->description}}</td>
                                                    <td>{{$permission->created_at}}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        @endif

                        {{-- Listado de Usuarios --}}
                        <div class="card">
                            <div class="card-header card-header-success">
                                <h4 class="card-tittle">Usuarios Asignados</h4>
                                <p class="card-category">Usuarios que cuenta con este Rol</p>
                            </div>

                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead class="text-primary">
                                            <th>Nombre</th>
                                            <th>Correo</th>
                                            <th>Fecha de Creacion</th>
                                        </thead>
                                        <tbody>
                                            @foreach ($rol->users as $user)
                                                <tr>
                                                    <td>{{$user->name}} {{$user->apellidoPa}} {{$user->apellidoMa}}</td>
                                                    <td>{{$user->email}}</td>
                                                    <td>{{$user->created_at}}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
