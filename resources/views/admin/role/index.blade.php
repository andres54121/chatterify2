@extends('layouts.main', ['activePage' => 'roles', 'titlePage' => 'Lista de Roles'])
@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header card-header-primary">
                                <h4 class="card-tittle">Roles</h4>
                                <p class="card-category">Roles Registrados</p>
                            </div>

                            <div class="card-body">
                                @if (session('success'))
                                    <div class="alert alert-success" role="success">
                                        {{ session('success')}}

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    </div>
                                @endif
                                <div class="row">
                                    <div class="col-12 text-right">
                                        <a href="{{route("roles.create")}}" class="btn btn-sm btn-facebook">Nuevo Rol</a>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead class="text-primary">
                                            <th>Nombre</th>
                                            <th>Descripcion</th>
                                            <th>Fecha de Creacion</th>
                                            <th class="text-right">Acciones</th>
                                        </thead>
                                        <tbody>
                                            @foreach ($roles as $rol)
                                            <tr>
                                            @if ($rol->description != 'Cliente')
                                                <td>{{$rol->name}}</td>
                                                <td>{{$rol->description}}</td>
                                                <td>{{$rol->created_at}}</td>
                                                <td class="td-actions text-right">

                                                    <a href="{{route("roles.show", $rol->id)}}" class="btn btn-info" title="Ver más..." rel="tooltip">
                                                        <i class="material-icons">person</i>
                                                    </a>
                                                    @if ($rol->name != 'SuperAdmin')
                                                        <a href="{{route("roles.edit", $rol->id)}}" class="btn btn-warning" title="Editar" rel="tooltip">
                                                            <i class="material-icons">edit</i>
                                                        </a>
                                                        <form action="{{route("roles.destroy", $rol->id)}}" method="post" style="display: inline-block;" onsubmit="return confirm('¿Esta seguro de eliminar este Rol?')">
                                                            @csrf
                                                            @method('DELETE')
                                                            <button class="btn btn-danger" type="submit" title="Eliminar" rel="tooltip">
                                                                <i class="material-icons">close</i>
                                                            </button>
                                                        </form>
                                                    @endif
                                                </td>
                                            @endif
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="card-footter ml-auto">
                                {{ $roles->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script src="{{ asset('js/material-kit.js?v=2.0.5') }}" type="text/javascript"></script>
@endsection
