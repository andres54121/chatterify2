@extends('layouts.main', ['activePage' => 'pedidos', 'titlePage' => 'Lista de Pedidos'])
@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header card-header-primary">
                                <h4 class="card-tittle">Pedidos</h4>
                                <p class="card-category">Lista de compras de los usuarios</p>
                            </div>

                            <div class="card-body">
                                @if (session('success'))
                                    <div class="alert alert-success" role="success">
                                        {{ session('success')}}

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    </div>
                                @endif
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead class="text-primary">
                                            <th>Codigo</th>
                                            <th>Estado</th>
                                            <th>Fecha de pedido</th>
                                            <th>Fecha de Entrega</th>
                                            <th>Total</th>
                                            <th class="text-right">Acciones</th>
                                        </thead>
                                        <tbody>
                                            <!-- recorriendo cada item del pedido-->
                                            @foreach ($users as $user)
                                                @foreach ( $user->order as $order )
                                                    <tr>
                                                        <td>{{ $order->code }}-{{ $user->id }}</td>
                                                        <th>{{ $order->status }}</th>
                                                        <td>{{ $order->order_date }}</td>
                                                        <td>{{ $order->arrived_date }}</td>
                                                        <td>${{ number_format($order->total, 2) }} </td>
                                                        <td class="td-actions text-right">
                                                            <button type="button" rel="tooltip" title="Detalles" class="btn btn-info btn-fab btn-fab-mini btn-round" data-toggle="modal" data-target="#productsModal{{ $order->code }}">
                                                                <i class="material-icons">list</i>
                                                            </button>
                                                            @if (empty ( $order->arrived_date))
                                                                <button type="button" rel="tooltip" title="Cambiar status" class="btn btn-success btn-fab btn-fab-mini btn-round" data-toggle="modal" data-target="#statusModal{{ $order->code }}">
                                                                        <i class="material-icons">assignment</i>
                                                                </button>
                                                            @endif
                                                            {{-- <button type="button" rel="tooltip" title="Enviar mensaje" class="btn btn-light btn-fab btn-fab-mini btn-round" data-toggle="modal" data-target="#messageModal{{ $order->code }}">
                                                                    <i class="material-icons">reply</i> --}}
                                                            </button>
                                                            @include('layouts.Shoppingcart.modalProducto')
                                                            @include('layouts.Shoppingcart.modalStatus')

                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="card-footter ml-auto">
                                {{-- {{ $roles->links() }} --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
    <script src="{{ asset('js/material-kit.js?v=2.0.5') }}" type="text/javascript"></script>
@endsection
