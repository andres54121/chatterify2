@extends('layouts.main', ['activePage' => 'plantas', 'titlePage' => 'Editar Planta'])

@section('css')
    <link href="{{ asset('/css/previewIMG.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <form action="{{ route('plantas.update', $planta->id) }}" method="post" class="form-horizontal" enctype="multipart/form-data">

                    @csrf
                    @method('PUT')
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-tittle">Planta</h4>
                            <p class="card-category">Editar Planta</p>
                        </div>

                        <div class="card-body">
                            @include('admin.articulos.planta.form')
                        </div>
                        <div class="card-footer ml-auto mr-auto">
                            <button type="submit" class="btn btn-primary">Guardar</button>
                        </div>
                    </div>
                </form>
                {{-- Imagenes de la planta --}}
                <div class="card">
                    <div class="card-header card-header-success">
                        <h4 class="card-tittle">Imagenes</h4>
                        <p class="card-category">Si desea eliminar una imagen haga clic en la X</p>
                    </div>
                    <div class="card-body">
                        <div>
                            @if ( !empty ( $planta->Imagenes) )
                                <span>Imagen(es) Actual(es): </span>
                                <br>

                                <!-- Mensaje: Imagen Eliminada Satisfactoriamente ! -->
                                @if(Session::has('message'))
                                    <div class="alert alert-primary" role="alert">
                                        {{ Session::get('message') }}
                                    </div>
                                @endif

                                <!-- Mostramos todas las imágenes pertenecientesa a este registro -->
                                <div class="row">
                                    @foreach($imagenes as $img)
                                        <div class=" col-md-4">
                                            <img src="../../../uploads/planta/{{ $img->nombre }}" width="200" class="img-fluid">

                                            <!-- Botón para Eliminar la Imagen individualmente -->
                                            <a href="{{ route('plantas/eliminarimagen', [$img->id]) }}" class="close"
                                                onclick="return confirm('¿Esta seguro de eliminar esta imagen?');" >
                                                <span aria-hidden="true">&times;</span>
                                            </a>
                                        </div>
                                    @endforeach
                                </div>
                           @else
                                Aún no se ha cargado una imagen para este producto
                           @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
    <script src="{{ asset('js/previewIMG.js') }}"></script>
@endsection
