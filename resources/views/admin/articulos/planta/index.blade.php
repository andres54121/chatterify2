@extends('layouts.main', ['activePage' => 'plantas', 'titlePage' => 'Listado de Plantas'])
@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header card-header-primary">
                                <h4 class="card-tittle">Plantas</h4>
                                <p class="card-category">Plantas en Ventas</p>
                            </div>

                            <div class="card-body">
                                @if (session('success'))
                                    <div class="alert alert-success" role="success">
                                        {{ session('success')}}

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    </div>
                                @endif
                                <div class="row">
                                    <div class="col-12 text-right">
                                        <a href="{{route("plantas.create")}}" class="btn btn-sm btn-success">Agregar Planta</a>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead class="text-primary">
                                            <th>Nombre</th>
                                            <th>Tipo</th>
                                            <th>Cantidad</th>
                                            <th>Precio</th>
                                            <th>Descripcion</th>
                                            <th>Estado</th>
                                            <th class="text-right">Acciones</th>
                                        </thead>
                                        <tbody>
                                            @foreach ($plantas as $Planta)
                                            <tr>
                                                <td>{{$Planta->Nombre}}</td>
                                                <td>{{$Planta->Tipo}}</td>
                                                <td>{{$Planta->Cantidad}}</td>
                                                <td>${{number_format($Planta->Precio, 2)}}</td>
                                                <td>{{$Planta->Descripcion}}</td>
                                                <td>
                                                    {{$Planta->Estado}}
                                                </td>
                                                <td class="td-actions text-right">
                                                    {{-- <button class="btn btn-info" type="button">
                                                        <i class="material-icons">person</i>
                                                    </button> --}}

                                                    <form action="{{route("plantas/changeStatus", $Planta->id)}}" method="post" style="display: inline-block;" onsubmit="return confirm('¿Esta seguro de cambiar el estado de esta planta?')">
                                                        @csrf
                                                        @if ($Planta->Estado == 'Habilitado')
                                                            <button class="btn btn-danger " type="submit" title="Cambiar status" rel="tooltip">
                                                                <i class="material-icons">change_circle</i>
                                                            </button>
                                                        @else
                                                            <button class="btn btn-info" type="submit" title="Cambiar status" rel="tooltip">
                                                                <i class="material-icons">change_circle</i>
                                                            </button>
                                                        @endif
                                                    </form>
                                                    <a href="{{route("plantas.edit", $Planta->id)}}" class="btn btn-warning" title="Editar" rel="tooltip">
                                                        <i class="material-icons">edit</i>
                                                    </a>
                                                    <form action="{{route("plantas.destroy", $Planta->id)}}" method="post" style="display: inline-block;" onsubmit="return confirm('¿Esta seguro de eliminar esta planta?')">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button class="btn btn-danger" type="submit" title="Eliminar" rel="tooltip">
                                                            <i class="material-icons">close</i>
                                                        </button>
                                                    </form>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="card-footter ml-auto">
                                {{ $plantas->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script src="{{ asset('js/material-kit.js?v=2.0.5') }}" type="text/javascript"></script>
@endsection
