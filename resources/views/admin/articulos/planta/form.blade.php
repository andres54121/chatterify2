{{-- Formulario Para el Edit --}}
@if ( !empty ( $planta->id) )
    <div class="row">
        <div class="form-group col-md-4">
            <label for="Nombre">Nombre:</label>
            <input type="text" name="Nombre" class="form-control" placeholder="Ingrese el nombre..." autofocus required value="{{ old('Nombre', $planta->Nombre) }}">
            @if ($errors->has('Nombre'))
                <span class="error text-danger" for="input-Nombre">{{ $errors->first('Nombre') }}</span>
            @endif
        </div>

        <div class="form-group col-md-4">
            <label>Tipo de Planta</label>
            <select class="form-control" name="Tipo">
            <option value="Arbol">Árbol</option>
            <option value="Arbustos">Arbustos</option>
            <option value="Trepadoras">Trepadoras</option>
            <option value="Cactus">Cactus</option>
            <option value="Crasas">Crasas</option>
            <option value="Herbaceas">Herbáceas</option>
            <option value="Hortícolas">Hortícolas</option>
            <option value="Palmeras">Palmeras</option>
            </select>
            @if ($errors->has('Tipo'))
                <span class="error text-danger" for="input-Tipo">{{ $errors->first('Tipo') }}</span>
            @endif
        </div>

        <div class="form-group col-md-2">
            <label for="Cantidad">Cantidad:</label>
            <input type="number" name="Cantidad" class="form-control" placeholder="catidad actual del inventario..." required value="{{ old('Cantidad', $planta->Cantidad) }}">
            @if ($errors->has('Cantidad'))
                <span class="error text-danger" for="input-Cantidad">{{ $errors->first('Cantidad') }}</span>
            @endif
        </div>
        <div class="form-group col-md-2">
            <label for="Precio">Precio:</label>
            <input type="number" name="Precio" class="form-control" placeholder="$???" required value="{{ old('Precio', $planta->Precio) }}">
            @if ($errors->has('Precio'))
                <span class="error text-danger" for="input-Precio">{{ $errors->first('Precio') }}</span>
            @endif
        </div>
    </div>

    <div class="form-group ">
        <label for="Descripcion">Descripcion:</label>
        <textarea rows="6" class="form-control" placeholder="Esta planta..." name="Descripcion" required>{{ old('Descripcion', $planta->Descripcion) }}</textarea>
        @if ($errors->has('Descripcion'))
            <span class="error text-danger" for="input-Descripcion">{{ $errors->first('Descripcion') }}</span>
        @endif
    </div>
    <div class="form-group ">
        <label for="Cuidados">Cuidado Especifico:</label>
        <textarea rows="6" class="form-control" placeholder="Esta planta..." name="Cuidados">{{ old('Cuidados', $planta->Cuidados) }}</textarea>
        @if ($errors->has('Cuidados'))
            <span class="error text-danger" for="input-Cuidados">{{ $errors->first('Cuidados') }}</span>
        @endif
    </div>
@else
{{-- Formulario Para el Create --}}
    <div class="row">
        <div class="form-group col-md-4">
            <label for="Nombre">Nombre:</label>
            <input type="text" name="Nombre" class="form-control" placeholder="Ingrese el nombre..." autofocus required value="{{ old('Nombre') }}">
            @if ($errors->has('Nombre'))
                <span class="error text-danger" for="input-Nombre">{{ $errors->first('Nombre') }}</span>
            @endif
        </div>

        <div class="form-group col-md-4">
            <label>Tipo de Planta</label>
            <select class="form-control" name="Tipo">
            <option value="Arbol">Árbol</option>
            <option value="Arbustos">Arbustos</option>
            <option value="Trepadoras">Trepadoras</option>
            <option value="Cactus">Cactus</option>
            <option value="Crasas">Crasas</option>
            <option value="Herbaceas">Herbáceas</option>
            <option value="Hortícolas">Hortícolas</option>
            <option value="Palmeras">Palmeras</option>
            </select>
            @if ($errors->has('Tipo'))
                <span class="error text-danger" for="input-Tipo">{{ $errors->first('Tipo') }}</span>
            @endif
        </div>

        <div class="form-group col-md-2">
            <label for="Cantidad">Cantidad:</label>
            <input type="number" name="Cantidad" class="form-control" placeholder="catidad actual del inventario..." autofocus required value="{{ old('Cantidad') }}">
            @if ($errors->has('Cantidad'))
                <span class="error text-danger" for="input-Cantidad">{{ $errors->first('Cantidad') }}</span>
            @endif
        </div>
        <div class="form-group col-md-2">
            <label for="Precio">Precio:</label>
            <input type="number" name="Precio" class="form-control" placeholder="$???" autofocus required value="{{ old('Precio') }}">
            @if ($errors->has('Precio'))
                <span class="error text-danger" for="input-Precio">{{ $errors->first('Precio') }}</span>
            @endif
        </div>
    </div>

    <div class="form-group ">
        <label for="Descripcion">Descripcion:</label>
        <textarea rows="6" class="form-control" placeholder="Esta planta..." name="Descripcion" autofocus required>{{ old('Descripcion') }}</textarea>
        @if ($errors->has('Descripcion'))
            <span class="error text-danger" for="input-Descripcion">{{ $errors->first('Descripcion') }}</span>
        @endif
    </div>
    <div class="form-group ">
        <label for="Cuidados">Cuidado Especifico:</label>
        <textarea rows="6" class="form-control" placeholder="Esta planta..." name="Cuidados">{{ old('Descripcion') }}</textarea>
        @if ($errors->has('Cuidados'))
            <span class="error text-danger" for="input-Cuidados">{{ $errors->first('Cuidados') }}</span>
        @endif
    </div>

@endif

<input name="img[]" type="file" id="file" multiple="multiple">
@if ($errors->has('img.*'))
    <br><br>
    <span class="error text-danger" for="input-Descripcion">{{ $errors->first('img.*') }}</span>
@endif
@if ($errors->has('img'))
    <br><br>
    <span class="error text-danger" for="input-Descripcion">{{ $errors->first('img') }}</span>
@endif


<div id="container-input">
    <div class="wrap-file">
        <div id="preview-images"></div>
    </div>
</div>
