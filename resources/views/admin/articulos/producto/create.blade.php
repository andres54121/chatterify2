@extends('layouts.main', ['activePage' => 'productos', 'titlePage' => 'Nuevo Producto'])

@section('css')
    <link href="{{ asset('/css/previewIMG.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <form action="{{route("productos.store")}}" method="post" class="form-horizontal" enctype="multipart/form-data">
                    @csrf
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-tittle">Producto</h4>
                            <p class="card-category">Ingresar Datos</p>
                        </div>

                        <div class="card-body">
                            @include('admin.articulos.producto.form')
                        </div>
                        <div class="card-footer ml-auto mr-auto">
                            <button type="submit" class="btn btn-primary">Guardar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script src="{{ asset('js/previewIMG.js') }}"></script>
@endsection
