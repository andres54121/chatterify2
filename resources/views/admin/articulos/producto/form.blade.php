{{-- Formulario Para el Edit --}}
@if ( !empty ( $producto->id) )
    <div class="row">
        <div class="form-group col-md-4">
            <label for="Nombre">Nombre:</label>
            <input type="text" name="Nombre" class="form-control" placeholder="Ingrese el nombre..." autofocus required value="{{ old('Nombre', $producto->Nombre) }}">
            @if ($errors->has('Nombre'))
                <span class="error text-danger" for="input-Nombre">{{ $errors->first('Nombre') }}</span>
            @endif
        </div>

        <div class="form-group col-md-2">
            <label for="Cantidad">Cantidad:</label>
            <input type="number" name="Cantidad" class="form-control" placeholder="catidad actual del inventario..." autofocus required value="{{ old('Cantidad', $producto->Cantidad) }}">
            @if ($errors->has('Cantidad'))
                <span class="error text-danger" for="input-Cantidad">{{ $errors->first('Cantidad') }}</span>
            @endif
        </div>
        <div class="form-group col-md-2">
            <label for="Precio">Precio:</label>
            <input type="number" name="Precio" class="form-control" placeholder="$???" autofocus required value="{{ old('Precio', $producto->Precio) }}">
            @if ($errors->has('Precio'))
                <span class="error text-danger" for="input-Precio">{{ $errors->first('Precio') }}</span>
            @endif
        </div>
    </div>

    <div class="form-group ">
        <label for="Descripcion">Descripcion:</label>
        <textarea class="form-control" placeholder="Esta producto..." name="Descripcion" autofocus required>{{$producto->Descripcion }}</textarea>
        @if ($errors->has('Descripcion'))
            <span class="error text-danger" for="input-Descripcion">{{ $errors->first('Descripcion') }}</span>
        @endif
    </div>
@else
{{-- Formulario Para el Create --}}
    <div class="row">
        <div class="form-group col-md-4">
            <label for="Nombre">Nombre:</label>
            <input type="text" name="Nombre" class="form-control" placeholder="Ingrese el nombre..." autofocus required value="{{ old('Nombre') }}">
            @if ($errors->has('Nombre'))
                <span class="error text-danger" for="input-Nombre">{{ $errors->first('Nombre') }}</span>
            @endif
        </div>

        <div class="form-group col-md-4">
            <label for="Cantidad">Cantidad:</label>
            <input type="number" name="Cantidad" class="form-control" placeholder="catidad actual del inventario..." autofocus required value="{{ old('Cantidad') }}">
            @if ($errors->has('Cantidad'))
                <span class="error text-danger" for="input-Cantidad">{{ $errors->first('Cantidad') }}</span>
            @endif
        </div>

        <div class="form-group col-md-4">
            <label for="Precio">Precio:</label>
            <input type="number" name="Precio" class="form-control" placeholder="$???" autofocus required value="{{ old('Precio') }}">
            @if ($errors->has('Precio'))
                <span class="error text-danger" for="input-Precio">{{ $errors->first('Precio') }}</span>
            @endif
        </div>
    </div>

    <div class="form-group ">
        <label for="Descripcion">Descripcion:</label>
        <textarea class="form-control" placeholder="Este Producto..." name="Descripcion" autofocus required>{{ old('Descripcion') }}</textarea>
        @if ($errors->has('Descripcion'))
            <span class="error text-danger" for="input-Descripcion">{{ $errors->first('Descripcion') }}</span>
        @endif
    </div>

@endif

<input name="img[]" type="file" id="file" multiple="multiple">
@if ($errors->has('img.*'))
    <br><br>
    <span class="error text-danger" for="input-Descripcion">{{ $errors->first('img.*') }}</span>
@endif
@if ($errors->has('img'))
    <br><br>
    <span class="error text-danger" for="input-Descripcion">{{ $errors->first('img') }}</span>
@endif

<div id="container-input">
    <div class="wrap-file">
        <div id="preview-images"></div>
    </div>
</div>
