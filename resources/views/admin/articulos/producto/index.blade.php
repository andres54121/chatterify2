@extends('layouts.main', ['activePage' => 'productos', 'titlePage' => 'Listado de Productos'])
@section('content')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header card-header-primary">
                                <h4 class="card-tittle">Productos</h4>
                                <p class="card-category">Productos en Ventas</p>
                            </div>

                            <div class="card-body">
                                @if (session('success'))
                                    <div class="alert alert-success" role="success">
                                        {{ session('success')}}

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    </div>
                                @endif
                                <div class="row">
                                    <div class="col-12 text-right">
                                        <a href="{{route("productos.create")}}" class="btn btn-sm btn-success">Agregar Producto</a>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead class="text-primary">
                                            <th>Nombre</th>
                                            <th>Cantidad</th>
                                            <th>Precio</th>
                                            <th>Descripcion</th>
                                            <th>Estado</th>
                                            <th class="text-right">Acciones</th>
                                        </thead>
                                        <tbody>
                                            @foreach ($productos as $producto)
                                            <tr>
                                                <td>{{$producto->Nombre}}</td>
                                                <td>{{$producto->Cantidad}}</td>
                                                <td>${{number_format($producto->Precio, 2)}}</td>
                                                <td>{{$producto->Descripcion}}</td>
                                                <td>{{$producto->Estado}}</td>
                                                <td class="td-actions text-right">
                                                    <form action="{{route("productos/changeStatus", $producto->id)}}" method="post" style="display: inline-block;" onsubmit="return confirm('¿Esta seguro de cambiar el estado de este producto?')">
                                                        @csrf
                                                        @if ($producto->Estado == 'Habilitado')
                                                            <button class="btn btn-danger" type="submit" title="Cambiar status" rel="tooltip">
                                                                <i class="material-icons">change_circle</i>
                                                            </button>
                                                        @else
                                                            <button class="btn btn-info" type="submit" title="Cambiar status" rel="tooltip">
                                                                <i class="material-icons">change_circle</i>
                                                            </button>
                                                        @endif
                                                    </form>
                                                    <a href="{{route("productos.edit", $producto->id)}}" class="btn btn-warning" title="Editar" rel="tooltip">
                                                        <i class="material-icons">edit</i>
                                                    </a>
                                                    <form action="{{route("productos.destroy", $producto->id)}}" method="post" style="display: inline-block;" onsubmit="return confirm('¿Esta seguro de eliminar este producto?')">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button class="btn btn-danger" type="submit" title="Eliminar" rel="tooltip">
                                                            <i class="material-icons">close</i>
                                                        </button>
                                                    </form>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="card-footter ml-auto">
                                {{ $productos->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modalAddToCart" tabindex="-1" role="dialog" aria-labelledby="modalAddToCart" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Indica la cantidad de productos</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

        <!-- Form para poder enviar los datos -->
        <form method="post" action="{{ url('/cart') }}">
          {{ csrf_field() }}
          <!-- campo oculto donde se envia el id del producto -->
          {{-- <input type="hidden" name="product_id" value="{{ $producto->id }}"> --}}
          <input type="hidden" name="product_id" value="1">

          <div class="modal-body">
            <input type="number" name="quantity" class="form-control" value="1">
          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
            <button type="submit" class="btn btn-primary">Añadir</button>
          </div>
        </form>

      </div>
    </div>
</div>

@endsection

@section('scripts')
    <script src="{{ asset('js/material-kit.js?v=2.0.5') }}" type="text/javascript"></script>
@endsection
