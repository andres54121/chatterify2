@extends('layouts.Shoppingcart')

@section('title','Perfil de Usuario')
<!-- si se quiere agregar una clase que esta en el tag body: -->
@section('body-class', 'profile-page sidebar-collapse')
<!-- estilos solo para esta pagina que no se aplicaron-->
@section('styless')
@endsection

@section('content')

    <!--jumbotron-->
    <div class="page-header header-filter" data-parallax="true" style="background-image: url({{ asset('img/cover.jpg') }})">
    </div>
    <!-- end jumbotron-->

    <div class="main main-raised">
        <div class="profile-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 ml-auto mr-auto">
                        <div class="profile">
                            <div class="avatar">
                                <img src="../../../img/nenufar.jpg"
                                    alt="Circle Image" class="img-raised rounded-circle img-fluid">
                            </div>
                            <div class="name">
                                <h3 class="title">{{ Auth::user()->name }} {{ Auth::user()->apellidoPa }} {{ Auth::user()->apellidoMa }}</h3>
                                <a href="{{ route('editProfile') }}" class="btn btn-primary">
                                    <i class="material-icons">edit</i> Editar Perfil
                                </a>
                                <a href="{{ url('/changePassword') }}" type="button" rel="tooltip" title="Cambiar Contraseña" class="btn btn-primary btn-fab btn-fab-mini btn-round" >
                                        <i class="material-icons">edit</i>
                                </a>
                            </div>
                        </div>
                        @if(session('notification'))
                            <div class="alert alert-success text-center">
                                {{ session('notification') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 ml-auto mr-auto">
                        <div class="profile-tabs">
                            <ul class="nav nav-pills nav-pills-icons justify-content-center" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" href="#infoUser" role="tab" data-toggle="tab">
                                        <i class="material-icons">camera</i>
                                        Info
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#orders" role="tab" data-toggle="tab">
                                        <i class="material-icons">schedule</i>
                                        Pedidos
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="tab-content tab-space">
                    <div class="tab-pane active text-center gallery" id="infoUser">
                        <div class="container text-center">
                            <h3  class="title">Correo Electronico</h3>
                            <h5>{{ Auth::user()->email }}</h5>
                        </div>
                        <div class="container text-center">
                            @if (!empty(Auth::user()->phone ))
                                <h3  class="title">Telefono</h3>
                                <h5>{{ Auth::user()->phone }}</h5>
                            @else
                                <h3  class="title">Telefono
                                    <i class=" icon icon-danger text-center material-icons">info</i>
                                </h3>
                                <div class="text-center">
                                    <h4>Vaya! parece que aun no has agregado un número de telefono</h4>
                            </div>
                            @endif
                        </div>
                        @if (!empty(Auth::user()->id_direccion ))
                            <h3  class="title">Direccion</h3>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Calle Principal</th>
                                        <th>Calles</th>
                                        <th>SMza</th>
                                        <th>Mza</th>
                                        <th>Lt</th>
                                        <th>CP</th>
                                        <th>Colonia</th>
                                        <th>Descripcion</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th>{{ $address->CalleP }}</th>
                                        <th>{{ $address->entreCalles }}</th>
                                        <td>{{ $address->Mza }}</td>
                                        <td>{{ $address->SMza }}</td>
                                        <td>{{ $address->Lt }}</td>
                                        <td>{{ $address->CP }}</td>
                                        <td>{{ $address->Colonia }}</td>
                                        <td>{{ $address->Descripcion }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        @else
                            <h3  class="title">Direccion
                                <i class=" icon icon-danger text-center material-icons">info</i>
                            </h3>
                            <div class="text-center">
                                <h4>Vaya! parece que aun no has agregado una dirección</h4>
                            </div>
                        @endif
                    </div>
                    @include('layouts.Shoppingcart.panelOrder')
                </div>
                {{-- Info Usuario --}}
                {{-- End Info --}}
            </div>
        </div>
    </div>

@endsection


@section('scripts')
<script src="{{ asset('/js/typeahead.bundle.min.js') }}"> </script>
@endsection
