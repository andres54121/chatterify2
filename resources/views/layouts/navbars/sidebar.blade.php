<div class="sidebar" data-color="orange" data-background-color="white" data-image="{{ asset('material') }}/img/sidebar-1.jpg">
  <!--
      Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

      Tip 2: you can also add an image using data-image tag
  -->
  <div class="logo">
    <a href="{{ route('home') }}" class="simple-text logo-normal">
      {{ __('Chatterify') }}
    </a>
  </div>
  <div class="sidebar-wrapper">
    <ul class="nav">
      <li class="nav-item{{ $activePage == 'dashboard' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('dashboard') }}">
          <i class="material-icons">dashboard</i>
            <p>{{ __('Dashboard') }}</p>
        </a>
      </li>
      {{-- Usuarios, Roles y permisos --}}

      @if(@Auth::user()->hasRole('SuperAdmin'))
        <li class="nav-item {{ ($activePage == 'role' || $activePage == 'permissions') ? ' active' : '' }}">
            <a class="nav-link" data-toggle="collapse" href="#role-permission" aria-expanded="true">
            <i><img style="width:25px" src="{{ asset('img/laravel.svg') }}"></i>
            <p>Administracion
                <b class="caret"></b>
            </p>
            </a>
            <div class="collapse show" id="role-permission">
            <ul class="nav">
                <li class="nav-item{{ $activePage == 'users' ? ' active' : '' }}">
                    <a class="nav-link" href="{{route("usuario.index")}}">
                    <span class="sidebar-mini"> LU </span>
                    <span class="sidebar-normal"> Listado de Usuarios </span>
                    </a>
                </li>
                <li class="nav-item{{ $activePage == 'roles' ? ' active' : '' }}">
                <a class="nav-link" href="{{route("roles.index")}}">
                    <span class="sidebar-mini"> LR </span>
                    <span class="sidebar-normal">Listado de Roles </span>
                </a>
                </li>
                <li class="nav-item{{ $activePage == 'permissions' ? ' active' : '' }}">
                <a class="nav-link" href="{{route("permisos.index")}}">
                    <span class="sidebar-mini"> LP </span>
                    <span class="sidebar-normal"> Listado de Permisos </span>
                </a>
                </li>
            </ul>
            </div>
        </li>
      @endif
      {{--  --}}
      {{-- <li class="nav-item{{ $activePage == 'permission' ? ' active' : '' }}">
        <a class="nav-link" href="{{route("permisos.index")}}">
          <i class="material-icons">library_books</i>
            <p>Permisos</p>
        </a>
      </li> --}}
      <li class="nav-item{{ $activePage == 'plantas' ? ' active' : '' }}">
        <a class="nav-link" href="{{route("plantas.index")}}">
          <i class="material-icons">library_books</i>
            <p>Listado de Plantas</p>
        </a>
      </li>
      <li class="nav-item{{ $activePage == 'productos' ? ' active' : '' }}">
        <a class="nav-link" href="{{route("productos.index")}}">
          <i class="material-icons">library_books</i>
            <p>Listado de Productos</p>
        </a>
      </li>
      <li class="nav-item{{ $activePage == 'pedidos' ? ' active' : '' }}">
        <a class="nav-link" href="{{route("pedidos")}}">
          <i class="material-icons">schedule</i>
            <p>Listado de Pedidos</p>
        </a>
      </li>
    </ul>
  </div>
</div>
