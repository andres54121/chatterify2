<!---Menu de navegacion-->
<nav class="navbar navbar-transparent navbar-color-on-scroll fixed-top navbar-expand-lg" color-on-scroll="100" id="sectionsNav">
    <div class="container">
        <div class="navbar-translate">

            <a class="navbar-brand" href="{{ url('/')}}">
                <svg class="bi me-2" width="40" height="32" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 55.86 39.22">
                    <defs>
                        <style>
                            .cls-1 {
                                isolation: isolate;
                            }

                            .cls-2 {
                                mix-blend-mode: multiply;
                                fill: url(#New_Gradient_Swatch_copy_63);
                            }

                        </style>
                        <radialGradient id="New_Gradient_Swatch_copy_63" cx="34.06" cy="6.38" r="88.39" gradientTransform="translate(0 -7.88) scale(1 1.16)" gradientUnits="userSpaceOnUse">
                            <stop offset="0" stop-color="#e72167" />
                            <stop offset="1" stop-color="#b22153" />
                        </radialGradient>
                    </defs>
                    <g class="cls-1">
                        <g id="Capa_1" data-name="Capa 1">
                            <path class="cls-2" d="M48.78,27.82c0,.78-.11,1.55-.22,2.29a21.36,21.36,0,0,1-1.9,6c1.37-.87,1.3-.78-.07.14a18.46,18.46,0,0,1-5.21,6.46A15.69,15.69,0,0,1,32.28,46h0a16,16,0,0,0,8.85-4,20.38,20.38,0,0,0,6.38-13.18c.46-4.94-1.17-10.54.56-15.07-2.18,3.4-6.14,4.67-9.42,6.75.31.73.6,1.46.83,2.2a21.72,21.72,0,0,1,1,6.12c.79-1.53.77-1.42,0,.15v.42a21.84,21.84,0,0,1-1.71,8.42,19.5,19.5,0,0,1-5.65,7.49,20.13,20.13,0,0,0,5.23-8.06,22.4,22.4,0,0,0-.24-15.13c-1.75-4.53-5.6-8.39-6.07-13.33-.47,4.94-4.32,8.8-6.07,13.33a22.4,22.4,0,0,0-.24,15.13,20.13,20.13,0,0,0,5.23,8.06,19.5,19.5,0,0,1-5.65-7.49,21.84,21.84,0,0,1-1.71-8.42v-.42c-.77-1.57-.79-1.68,0-.15a21.72,21.72,0,0,1,.95-6.12c.23-.74.52-1.47.83-2.2-3.28-2.08-7.24-3.35-9.42-6.75,1.73,4.53.1,10.13.56,15.07A20.38,20.38,0,0,0,22.85,42a16,16,0,0,0,8.85,4h0a15.69,15.69,0,0,1-9.1-3.23,18.46,18.46,0,0,1-5.21-6.46c-1.37-.92-1.44-1-.07-.14a21.36,21.36,0,0,1-1.9-6c-.11-.74-.18-1.51-.22-2.29-3.76.12-7.77,1.4-11.15-.26,3.47,2.89,4.49,8.68,7,12.66a16.38,16.38,0,0,0,11.24,7.61A15.25,15.25,0,0,0,31.86,46h-.14l.14,0-.14-.1L32,27.28c0-2,0-4.54,0-7.16,0,2.62,0,5.21,0,7.16l.24,18.59-.14.1.14,0h-.14a15.25,15.25,0,0,0,9.51,1.86,16.38,16.38,0,0,0,11.24-7.61c2.55-4,3.57-9.77,7-12.66C56.55,29.22,52.54,27.94,48.78,27.82Z" transform="translate(-4.07 -8.74)" />
                        </g>
                    </g>
                </svg>
                Chatterify
            </a>

            <!-- Boton desplegable para responsive-->
            <button class="navbar-toggler" type="button" data-toggle="collapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="sr-only">Toggle navigation</span>
                <span class="navbar-toggler-icon"></span>
                <span class="navbar-toggler-icon"></span>
                <span class="navbar-toggler-icon"></span>
            </button>
        </div>

        <!-- opciones navegacion -->
        <div class="collapse navbar-collapse">
            <ul class="navbar-nav ml-auto">

                <li class="nav-item"><a class="nav-link" href="{{ url('/') }}">Inicio</a></li>
                <li class="dropdown nav-item">
                    <a class="nav-link" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true" v-pre>
                        Catalogo
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="dropdown nav-item">
                            <a class="nav-link" href="/catalogo/plantas">
                                Plantas
                            </a>
                        </li>
                        <li class="dropdown nav-item">
                            <a class="nav-link" href="/catalogo/herramientas">
                                Herramientas
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item"><a class="nav-link" href="/info">¿Quienes Somos?</a></li>

                <!-- Authentication Links -->
                @guest
                <li class="nav-item">
                    <a href="{{ route('register') }}" class="nav-link">
                        <i class="material-icons">person_add</i> Registrarse
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('login') }}" class="nav-link">
                        <i class="material-icons">fingerprint</i> Iniciar Sesión
                    </a>
                </li>
                @else
                <li class="dropdown nav-item">
                    <a class="nav-link" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true" v-pre>
                        {{ Auth::user()->name }}
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="dropdown nav-item">
                            <a class="nav-link" href="/profile">
                                Perfil
                            </a>
                        </li>
                        <li class="dropdown nav-item">
                            <a class="nav-link" href="/Shoppingcart">
                                Carrito de Compras
                            </a>
                        </li>

                        <li class="dropdown nav-item">
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                            <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">CERRAR SESIÓN</a>
                        </li>
                    </ul>
                </li>
                @endguest
                <!-- END Authentication Links -->

            </ul>
        </div>
    </div>
</nav>
<!---End Menu de navegacion-->
