<!--Start Footer-->
<footer class="footer">
    <div class="container">
      <nav class="float-left">
        <ul>
            <li>
                <a href="#">
                  Telfono de Contacto:
                </a>
            </li>
            <li>
                <a href="#">
                    (998) 333 4444
                </a>
            </li>
        </ul>
      </nav>

      <div class="copyright float-right">
        &copy;
        <script>
          document.write(new Date().getFullYear())
        </script>, Desarrollado por IDYGS81<i class="material-icons">favorite</i> by
        <a href="https://www.creative-tim.com" target="_blank">Creative Tim</a>.
      </div>

    </div>
</footer>
<!--end footer-->
