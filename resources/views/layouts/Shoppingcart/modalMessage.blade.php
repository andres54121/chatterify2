<!-- Modal Menssage -->
<div class="modal fade" id="messageModal" tabindex="-1" role="dialog" aria-labelledby="messageModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Enviar mensaje </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
        <form class="contact-form" method="post" action="#">

                <div class="form-group">
                    <label class="bmd-label-floating">Asunto</label>
                    <input type="text" class="form-control">
                </div>

                <div class="form-group">
                    <label class="bmd-label-floating" value="">Correo Electronico</label>
                    <input type="email" class="form-control">
                </div>

                <div class="form-group">
                    <label for="exampleMessage" class="bmd-label-floating">Mensaje</label>
                    <textarea type="email" class="form-control" rows="4" id="exampleMessage"></textarea>
                </div>

                <div class="row">
                <div class="col-md-4 ml-auto mr-auto text-center">
                    <button class="btn btn-primary btn-raised">
                    Enviar
                    </button>
                </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
        </div>
    </div>
</div>
