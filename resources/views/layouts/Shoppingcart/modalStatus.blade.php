<!-- Modal Status -->
<div class="modal fade" id="statusModal{{ $order->code }}" tabindex="-1" role="dialog" aria-labelledby="statusModal{{ $order->code }}" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle">Actualizar estado del pedido</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form method="post" action="{{ route('order/status', [$order->id]) }}">
              {{ csrf_field() }}
              <label for="inputState">Status</label>
              <select id="inputState" name="status" class="form-control">
                  <option selected>Pendiente</option>
                  <option>Aprobado</option>
                  <option>Finalizado</option>
                  <option>Cancelado</option>
              </select>
              <button type="submit" class="btn btn-primary ml-auto mr-auto text-center">Actualizar</button>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
    </div>
</div>
