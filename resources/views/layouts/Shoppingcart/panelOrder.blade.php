<!-- Pedidos -->
<div class="tab-pane table-responsive-sm " id="orders">
    @if (auth()->user()->order->count() > 0)
        <table class="table">
            <thead>
                <tr>
                    <th>Codigo</th>
                    <th>Status</th>
                    <th>Orden</th>
                    <th>Recibido</th>
                    <th>Total</th>
                    <th class="text-rigth">Acciones</th>
                </tr>
            </thead>
            <tbody>
                <!-- recorriendo cada item del pedido-->
                @foreach (auth()->user()->order as $order)
                    <tr>
                        <td>{{ $order->code }}</td>
                        <th>{{ $order->status }}</th>
                        <td>{{ $order->order_date }}</td>
                        <td>{{ $order->arrived_date }}</td>
                        <td class="td-actions ">&dollar; {{ number_format($order->total, 2) }} </td>
                        <td class="td-actions">
                            <button type="button" rel="tooltip" title="Detalles"
                                class="btn btn-info btn-fab btn-fab-mini btn-round" data-toggle="modal"
                                data-target="#productsModal{{ $order->code }}">
                                <i class="material-icons">list</i>
                            </button>

                            {{-- <button type="submit" rel="tooltip" title="Cancelar pedido" class="btn btn-danger btn-fab btn-fab-mini btn-round"data-toggle="modal" data-target="#confirmModal">
                    <i class="fa fa-times"></i> --}}
                            </button>
                            @include('layouts.Shoppingcart.modalProducto')
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @else
        <div class="row">
            <div class="col-md-12">
                <div class="info">
                    <div class="icon icon-info text-center">
                        <i class="material-icons">info</i>
                    </div>
                    <div class="text-center">
                        <h2>No hay pedidos aqui por ahora</h2>
                    </div>
                </div>
            </div>
        </div>
    @endif
</div>
<!-- End Pedidos -->
