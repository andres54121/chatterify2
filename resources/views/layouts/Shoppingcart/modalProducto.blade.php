@if (!empty ( $user))
<!-- Modal Products -->
<div class="modal fade" id="productsModal{{ $order->code }}" tabindex="-1" role="dialog" aria-labelledby="productsModal{{ $order->code }}" aria-hidden="true">
<div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
          <h5 class="text-center modal-title " id="exampleModalLongTitle">Mostrando productos del pedido</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
          </button>
      </div>
      <div class="modal-body">
          <table class="table">
              <thead>
                  <tr>
                      <th>Productos</th>
                      <th>Precio</th>
                      <th>Cantidad</th>
                  </tr>
              </thead>
              <tbody>
                  @if($user->orderDetails)
                      @foreach($order->details as $detail)
                      <tr>

                        @if (empty ($detail->product->id))
                            <td>{{ $detail->planta->Nombre }}</td>
                            <td>&dollar;{{ number_format($detail->planta->Precio, 2) }}</td>
                            <td>{{ $detail->quantity }}</td>
                        @else
                            <td>{{ $detail->product->Nombre }}</td>
                            <td>&dollar;{{ number_format($detail->product->Precio, 2) }}</td>
                            <td>{{ $detail->quantity }}</td>
                        @endif
                      </tr>
                      @endforeach
                  @endif

              </tbody>
          </table>
      </div>
  </div>
  <div class="modal-content">
    <div class="modal-header">
        <h5 class="text-center modal-title " id="exampleModalLongTitle">Direccion del pedido</h5>
    </div>
    <div class="modal-body text-left">
        @php $address = App\Models\Address::find($order->address_id); @endphp
        <li><strong>Calle Principal:</strong> {{ $address->CalleP }}</li>
        <li><strong>Entre Calles:</strong> {{ $address->entreCalles }}</li>
        <li><strong>Colonia:</strong> {{ $address->Colonia }}</li>
        <li>
            <strong>SMza:</strong> {{ $address->SMza }}
            <strong>Mza:</strong> {{ $address->Mza }}
            <strong>Lt:</strong>{{ $address->Lt }}
            <strong>CP:</strong> {{ $address->CP }}
        </li>
        <li><strong>Descripcion:</strong> {{ $address->Descripcion }}</li>
    </div>
</div>
</div>
</div>

@else
<div class="modal fade" id="productsModal{{ $order->code }}" tabindex="-1" role="dialog" aria-labelledby="productsModal{{ $order->code }}" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
              <h5 class="text-center modal-title " id="exampleModalLongTitle">Mostrando productos del pedido</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
              </button>
          </div>
          <div class="modal-body">
              <table class="table">
                  <thead>
                      <tr>
                          <th>Producto</th>
                          <th>Precio</th>
                          <th>Cantidad</th>
                      </tr>
                  </thead>
                  <tbody>
                      @if(auth()->user()->orderDetails)
                          @foreach($order->details as $detail)
                          <tr>

                            @if (empty ($detail->product->id))
                                <td>{{ $detail->planta->Nombre }}</td>
                                <td>&dollar;{{ number_format($detail->planta->Precio, 2) }}</td>
                                <td>{{ $detail->quantity }}</td>
                            @else
                                <td>{{ $detail->product->Nombre }}</td>
                                <td>&dollar;{{ number_format($detail->product->Precio, 2) }}</td>
                                <td>{{ $detail->quantity }}</td>
                            @endif
                          </tr>
                          @endforeach
                      @endif

                  </tbody>
              </table>
          </div>
      </div>
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="text-center modal-title " id="exampleModalLongTitle">Direccion del pedido</h5>
            </div>
            <div class="modal-body text-left">
                @php $address = App\Models\Address::find($order->address_id); @endphp
                <ul>
                    <li><strong>Calle Principal:</strong> {{ $address->CalleP }}</li>
                    <li><strong>Entre Calles:</strong> {{ $address->entreCalles }}</li>
                    <li><strong>Colonia:</strong> {{ $address->Colonia }}</li>
                    <li>
                        <strong>SMza:</strong> {{ $address->SMza }}
                        <strong>Mza:</strong> {{ $address->Mza }}
                        <strong>Lt:</strong>{{ $address->Lt }}
                        <strong>CP:</strong> {{ $address->CP }}
                    </li>
                    <br>
                    <li><strong>Descripcion:</strong> {{ $address->Descripcion }}</li>
                </ul>
            </div>
        </div>
    </div>
    </div>
<!-- Modal Products User-->
@endif
