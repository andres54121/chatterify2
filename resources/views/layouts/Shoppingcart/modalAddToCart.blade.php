
@guest
<!-- nothing -->
@else
<button class="btn btn-outline-primary  btn-round" data-toggle="modal" data-target="#modalAddToCart-{{ $Producto->id }}">
    <i class="material-icons">add</i> Añadir al carrito
</button>
@endguest

<!-- Modal -->
<div class="modal fade" id="modalAddToCart-{{ $Producto->id }}" tabindex="-1" role="dialog" aria-labelledby="modalAddToCart-{{ $Producto->id }}" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Indica la cantidad de {{$Producto->Nombre}}</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

        <!-- Form para poder enviar los datos -->
        <form method="post" action="{{ url('/cart') }}">
          {{ csrf_field() }}
          <!-- campo oculto donde se envia el id del producto -->
          <input type="hidden" name="product_id" value="{{ $Producto->id }}">

          <div class="modal-body">
            <input type="number" name="quantity" class="form-control" value="1">
          </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
            <button type="submit" class="btn btn-primary">Añadir</button>
          </div>
        </form>

      </div>
    </div>
</div>
