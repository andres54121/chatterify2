@extends('layouts.Shoppingcart')

@section('title','Chatterify')
<!-- si se quiere agregar una clase que esta en el tag body: -->
@section('body-class', 'landing-page sidebar-collapse')
<!-- estilos solo para esta pagina que no se aplicaron-->
@section('styless')
<style>
    .row {
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        flex-wrap: wrap;
    }

    .row>[class*='col-'] {
        display: flex;
        flex-direction: column;
    }

</style>
@endsection

@section('content')

<!--jumbotron-->
<div class="page-header header-filter" data-parallax="true" style="background-image: url({{ asset('https://cdn.pixabay.com/photo/2016/05/24/16/48/mountains-1412683_960_720.png') }})">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="title">Bienvenido a Chatterify tu tienda de plantas preferida.</h1>
                <h4>Realiza pedidos en linea y te contactaremos para coordinar la entrega</h4>
                <br>
            </div>
        </div>
    </div>
</div>
<!-- end jumbotron-->


<!--principal content, similar al landing page-->
<div class="main main-raised">
    <div class="container">
        <div class="section text-center">
            <div class="row">
                <div class="col-md-8 ml-auto mr-auto">
                    @if (session('notification'))
                    <div class="alert alert-success text-center">
                        {{ session('notification') }}
                    </div>
                    @endif
                    <h2 class="title">¿Qué es Chatterify ?</h2>
                    <h5 class="description">Somos una nueva tienda de plantas
                        de distintas categorias a precios accesibles. Puedes comparar precios y realizar tus pedidos
                        cuando lo desees! </h5>
                </div>
            </div>





            <div class="features mb-5">
            <h2 class="text-center title mt-5">Misión y Visión</h2>
            <div class="row">

                <div class="col-md-4">
                    <div class="info">

                        <h4 class="info-title">Misión</h4>
                        <p>Nuestro equipo atenderá rapidamente cualquier consulta que tengas via chat. ¡Estamos para servirte!</p>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="info">

                        <h4 class="info-title"> </h4>
                        <p></p>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="info">

                        <h4 class="info-title"> Visión</h4>
                        <p>¡Tu privacidad y seguridad son importantes para nosotros!. Los pedidos que realices solo los podras conocer tú mediante tu panel de usuario.</p>
                    </div>
                </div>
            </div>
        </div>










        <div class="features">
            <div class="row">

                <div class="col-md-4">
                    <div class="info">
                        <div class="icon icon-info">
                            <i class="material-icons">chat</i>
                        </div>
                        <h4 class="info-title">Atendemos tus dudas</h4>
                        <p>Nuestro equipo atenderá rapidamente cualquier consulta que tengas via chat. ¡Estamos para servirte!</p>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="info">
                        <div class="icon icon-success">
                            <i class="material-icons">verified_user</i>
                        </div>
                        <h4 class="info-title">Pago Seguro</h4>
                        <p>Todo pedido que realices será confirmado a traves de tu correo electronico.</p>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="info">
                        <div class="icon icon-danger">
                            <i class="material-icons">fingerprint</i>
                        </div>
                        <h4 class="info-title">Protegemos tus datos</h4>
                        <p>¡Tu privacidad y seguridad son importantes para nosotros!. Los pedidos que realices solo los podras conocer tú mediante tu panel de usuario.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>






</div>

<div class="container mb-5">

    <div class="section section-contacts  ">
        <div class="row">
            <div class="col-md-8 ml-auto mr-auto">
                <h2 class="text-center title">Trabajamos para ti</h2>
                <h4 class="text-center description">Puedes enviarnos tus dudas y te responderemos a la brevedad.</h4>
                <form class="contact-form" method="post" action="{{ url('/message/new') }}">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="bmd-label-floating">¿Cuál es tu nombre?</label>
                                <input type="text" name="name_user" class="form-control">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="bmd-label-floating">Tu Correo Electronico</label>
                                <input type="email" name="email" class="form-control">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="exampleMessage" class="bmd-label-floating">Plantea tus dudas</label>
                        <textarea type="text" name="message" class="form-control" rows="4" id="exampleMessage"></textarea>
                    </div>
                    <div class="row">
                        <div class="col-md-4 ml-auto mr-auto text-center">
                            <button class="btn btn-primary btn-raised">
                                Enviar Consulta
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>

</div>
</div>
<!--end principal content-->
@endsection

@section('scripts')
<script src="{{ asset('/js/typeahead.bundle.min.js') }}"> </script>
<script>
    $(function() {
        // Inicializamos el motor de busqueda que realmente hace las sugerencias
        // bundle: una agrupacion de ambos
        var products = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.whitespace
            , queryTokenizer: Bloodhound.tokenizers.whitespace
            , prefetch: '{{ url("/products/json") }}'
        });

        // Inicializamos typeahead sobre el input de busqueda
        $('#search').typeahead({
            //propiedades
            hint: true
            , highlight: true
            , minLength: 1
        }, {
            // dataset
            name: 'products'
            , source: products
        });

    });

</script>
@endsection
