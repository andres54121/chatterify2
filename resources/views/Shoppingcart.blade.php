@extends('layouts.Shoppingcart')

@section('title', 'Shopping Market')
@section('body-class', 'profile-page sidebar-collapse')
    @include("dashboard.cart")
    @include("dashboard.order")
    @include("dashboard.messages")


@section('content')

    <!--jumbotron-->
    <div class="page-header header-filter" data-parallax="true"
        style="background-image: url({{ asset('img/cart.jpg') }})">
    </div>
    <!-- end jumbotron-->


    <!-- Registro de producto -->
    <div class="main main-raised">
        <div class="section container">
            <h2 class="title text-center"> ¡Hola, {{ Auth::user()->name }}! </h2>
            <hr>
            @if (session('notification'))
                <div class="alert alert-success text-center">
                    {{ session('notification') }}
                </div>
            @endif

            <!-- start nav pills -->
            <ul class="nav nav-pills nav-pills-icons" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" href="#cart" role="tab" data-toggle="tab">
                        <i class="material-icons">add_shopping_cart</i>
                        Carrito
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="#orders" role="tab" data-toggle="tab">
                        <i class="material-icons">schedule</i>
                        Pedidos
                    </a>
                </li>
            </ul>


            <div class="tab-content tab-space ">
                <!--Carrito -->

                <div class="tab-pane active table-responsive-md" id="cart">
                    @if (auth()->user()->cart->details->count() > 0)
                        <hr>
                        <p> Tu carrito de compras tiene {{ auth()->user()->cart->details->count() }} productos </p>
                        @php  $total = 0; @endphp
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Ref</th>
                                    <th>Nombre</th>
                                    <th>Precio</th>
                                    <th>Cantidad</th>
                                    <th>Subtotal</th>
                                    <th class="text-rigth">Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!-- recorriendo cada item del carrito -->
                                @foreach (auth()->user()->cart->details as $detail)
                                    <tr>
                                        @if (empty($detail->product->id))
                                            <!-- Plantas -->
                                            @php
                                                $imgPlantas = App\Models\Articulos\imgPlantas::where('planta_id', '=', $detail->planta->id)->get();
                                                if (empty($imgPlantas[0]))
                                                    $nombre = 'Image.png';
                                                else
                                                    $nombre = $imgPlantas[0]->nombre;
                                                $total= $total + $detail->planta->Precio * $detail->quantity;
                                            @endphp
                                            <td><img src="../../../uploads/planta/{{ $nombre }}" alt="thumb" height="125"></td>
                                            <td> <a href="#"></a>{{ $detail->planta->Nombre }}</td>
                                            <td class="td-actions ">${{ number_format($detail->planta->Precio, 2) }}</td>
                                            <td> {{ $detail->quantity }}</td>
                                            <td> ${{ number_format($detail->quantity * $detail->planta->Precio, 2) }}</td>
                                            <td class="td-actions col-md-4">
                                                <!--/cart va hacia CartController-->
                                                <form method="POST" action="{{ url('/cart') }}">
                                                    {{ csrf_field() }}
                                                    {{ method_field('DELETE') }}

                                                    <input type="hidden" name="cart_detail_id" value="{{ $detail->id }}">
                                                    {{-- $detail->planta->id --}}
                                                    <a href="{{ url('/catalogo/plantas/'. $detail->planta->id) }}" target="_blank" rel="tooltip" title="Ver producto"
                                                        class="btn btn-info btn-fab btn-fab-mini btn-round">
                                                        <i class="material-icons">info</i>
                                                    </a>

                                                    <button type="submit" rel="tooltip" title="Quitar del carrito"
                                                        class="btn btn-danger btn-fab btn-fab-mini btn-round">
                                                        <i class="fa fa-times"></i>
                                                    </button>
                                                </form>
                                            </td>
                                        @else
                                            <!-- Productos -->
                                            @php
                                                $imgProductos = App\Models\Articulos\imgProductos::where('producto_id', '=', $detail->product->id)->get();
                                                if (empty($imgProductos[0]))
                                                    $nombre = 'Image.png';
                                                else
                                                    $nombre = $imgProductos[0]->nombre;
                                                $total= $total + $detail->product->Precio * $detail->quantity;
                                            @endphp
                                            <td><img
                                                    src="../../../uploads/producto/{{ $nombre }}"
                                                    alt="Thumbnail Image" height="75"></td>
                                            <td> <a href="#"></a>{{ $detail->product->Nombre }}</td>
                                            <td class="td-actions ">$ {{ number_format($detail->product->Precio, 2) }} </td>
                                            <td> {{ $detail->quantity }}</td>
                                            <td> ${{ $detail->quantity * $detail->product->Precio }}</td>
                                            <td class="td-actions col-md-4">
                                                <!--/cart va hacia CartController-->
                                                <form method="POST" action="{{ url('/cart') }}">
                                                    {{ csrf_field() }}
                                                    {{ method_field('DELETE') }}

                                                    <input type="hidden" name="cart_detail_id" value="{{ $detail->id }}">
                                                    {{-- $detail->product->id --}}
                                                    <a href="{{ url('/catalogo/herramientas/'. $detail->product->id) }}" target="_blank" rel="tooltip" title="Ver producto"
                                                        class="btn btn-info btn-fab btn-fab-mini btn-round">
                                                        <i class="material-icons">info</i>
                                                    </a>

                                                    <button type="submit" rel="tooltip" title="Quitar del carrito"
                                                        class="btn btn-danger btn-fab btn-fab-mini btn-round">
                                                        <i class="fa fa-times"></i>
                                                    </button>
                                                </form>
                                            </td>
                                        @endif
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>

                        <div class="text-center">
                            @if (!empty(auth()->user()->id_direccion))
                                <h3  class="title">Total a Pagar</h3>
                                <h5>${{ number_format($total, 2) }}</h5>
                                <form method="post" action="{{ route('payment') }}">
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-success btn-round">
                                        <i class="material-icons">done</i>Realizar Pedido
                                    </button>
                                </form>
                            @else
                                <div class="info">
                                    <div class="text-center">
                                        <h3>Añada una direccion a su perfil para continuar con la compra
                                            <i class="icon icon-info material-icons">info</i>
                                        </h3>
                                    </div>
                                </div>
                            @endif
                        </div>

                    @else
                        <div class="row">
                            <div class="col-md-12">
                                <div class="info">
                                    <div class="icon icon-info text-center">
                                        <i class="material-icons">info</i>
                                    </div>
                                    <div class="text-center">
                                        <h2>Vaya! parece que tu carrito está vacio</h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
                <!--End Carrito -->
                <!--Pedidos -->
                @include('layouts.Shoppingcart.panelOrder')
                <!-- Mensajes -->
                @if (auth()->user()->admin)
                    <div class="tab-pane" id="messages">
                        @yield("content_dashboard_messages")
                    </div>
                @endif
                <!-- End mensajes -->

            </div>
        </div>
    </div>
    <!--end principal content-->


@endsection

@section('scripts')
    <script src="{{ asset('js/material-dashboard') }}" type="text/javascript"></script>
@endsection
